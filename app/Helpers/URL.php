<?php

function resources($path) {
	return 'resources/assets/' . $path;
}

function urlAPI($path) {
//	return 'http://evaluacionpsicologica.es/EPAPI/public/EP/' . $path;
	return 'http://localhost/EP-API/public/EP/' . $path;
}

function urlDashboard($path) {
	return 'http://evaluacionpsicologica.es/dashboard/' . $path;
}
