<?php

namespace App\Repositories\CURL;

use App\CURL;

class CURLRepository {

	public function get($path) {
		$result = $this->requestCurl('GET', $path);
		return $result;
	}

	public function post($path, $data) {
		$result = $this->requestCurl('POST', $path, $data);
		return $result;
	}

	public function put($path) {
		$result = $this->requestCurl('PUT', $path);
		return $result;
	}

	public function delete($path) {
		$result = $this->requestCurl('DELETE', $path);
		return $result;
	}

	private function requestCurl($method, $path, $data = []) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, urlAPI($path));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		switch ($method) {
			case 'GET':
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
				break;
			case 'POST':
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
				break;
			case 'PUT':
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
				break;
			case 'DELETE':
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
				break;
		}

		// Set so curl_exec returns the result instead of outputting it.
		$result = curl_exec($ch);
		$resultJson = json_decode($result);
		curl_close($ch);

//		if ($path == 'preguntas-home') {
//			echo '<pre>';
//			print_r($result);
//			echo '</pre>';
//			die();
//		}

		if (!empty($resultJson) && !empty($resultJson->ok) && $resultJson->ok == 'true') {
			return $resultJson->response;
		}
		return $resultJson;
	}

}
