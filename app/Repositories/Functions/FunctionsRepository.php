<?php

namespace App\Repositories\Functions;

use App\Repositories\CURL\CURLRepository;
use App\Functions;

class FunctionsRepository {

	protected $curl;

	public function __construct(CURLRepository $curl) {
		$this->curl = $curl;
	}

	// Login
	public function login($data) {
		return $this->curl->post('authenticate', $data);
	}

	// Mails
	public function sendEmail($data) {
		return $this->curl->post('enviar-mensaje', $data);
	}

	// Search
	public function search($data) {
		return $this->curl->post('buscar', $data);
	}

	public function getMenu() {
		return $this->curl->get('menu');
	}

	public function getFaqsHome() {
		return $this->curl->get('preguntas-home');
	}

	public function getItemsHome() {
		return $this->curl->get('items-home');
	}

	public function getRandomHome() {
		return $this->curl->get('vistos');
	}

	//Sidebar
	public function getArchive($data) {
		return $this->curl->post('archivo', $data);
	}

	public function getLast() {
		return $this->curl->get('ultima');
	}

	public function getTimeline($data) {
		return $this->curl->post('ultimas', $data);
	}

	public function getGallery() {
		return $this->curl->get('galeria');
	}

	public function getSameCategory($data) {
		return $this->curl->post('misma-categoria', $data);
	}

	public function getSameSubcategory($data) {
		return $this->curl->post('misma-subcategoria', $data);
	}

	// Category
	public function getCategory($category) {
		return $this->curl->get($category);
	}

	public function getCategoryArchive($category) {
		return $this->curl->get('biblioteca/' . $category);
	}

	// Subcategory
	public function getSubcategory($category, $subcategory) {
		return $this->curl->get($category . '/' . $subcategory);
	}

	public function getSubcategoryArchive($category, $subcategory) {
		return $this->curl->get('biblioteca/' . $category . '/' . $subcategory);
	}

	// Items
	public function getItem($category, $subcategory, $path) {
		return $this->curl->get($category . '/' . $subcategory . '/' . $path);
	}

	// Teacher
	public function getTeachers() {
		return $this->curl->get('profesores');
	}

	public function getTeacher($path) {
		return $this->curl->get('profesores/' . $path);
	}

}
