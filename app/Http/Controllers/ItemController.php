<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;

class ItemController extends Controller {

	public function getItem($category, $subcategory, $path) {
		$data = $this->functions->getItem($category, $subcategory, $path);
		// If exist item
		if (!empty($data->item)) {
			// If wrong Url
			if ($data->item->path == $path) {
				// Fill data
				$data = (array) $data;
				// Return view
				return view('item.index', $data);
			} else {
				// Redirect if url is wrong
				return Redirect::to($data->item->url, 301);
			}
		} else {
			return $this->show404();
		}
	}

}
