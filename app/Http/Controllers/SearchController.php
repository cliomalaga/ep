<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends Controller {

	public function getSearchPage() {
		return view('page.search');
	}

	public function search(Request $request) {
		$post = $request->all();
		$data['items'] = $this->functions->search($post);
		$data['search'] = $post['search'];
		$data['searchPage'] = true;

		return view('page.search', $data);
	}

}
