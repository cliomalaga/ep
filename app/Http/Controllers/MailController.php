<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Session;

class MailController extends Controller {

	public function sendEmail(Request $request) {
		$post = $request->all();
		$response = $this->functions->sendEmail($post);
		if ($response == 'OK') {
			Session::flash($post['section'], 'Mensaje enviado correctamente.');
			return back();
		} else {
			Session::flash($post['section'], $response->message);
			return back()->withInput();
		}
	}

}
