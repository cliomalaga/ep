<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Repositories\Functions\FunctionsRepository;
use Session;

class Controller extends BaseController {

	use AuthorizesRequests,
	 DispatchesJobs,
	 ValidatesRequests;

	protected $functions;

	public function __construct(FunctionsRepository $functions) {
		$this->functions = $functions;
		$this->restringedUrls = ['documentos', 'bienvenido'];
	}

	public function show404() {
		// Redirect 404 if item not exist
		return view('404.index');
//		return view('404.index-full');
	}

	public function inRestringedUrls($url) {
		return in_array($url, $this->restringedUrls);
	}

	public static function isLogged() {
		$user = Session::get('user');
		if (empty($user) || empty($user->id)) {
			return false;
		}
		return true;
	}

	public function canShowUrl($url) {
		if ($this->inRestringedUrls($url) && !$this->isLogged()) {
			return false;
		}
		return true;
	}

	public function redirectLogin() {
		return redirect('login');
	}

}
