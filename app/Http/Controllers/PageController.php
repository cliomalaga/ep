<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller {

	// Static Pages
	// About Us
	public function getAboutUs() {
		return view('page.aboutus');
	}

	// Membership Page
	public function getMembershipPage() {
		return view('page.membership');
	}

	// Admission Membership Page
	public function getAdmissionMembershipPage() {
		return view('page.admission');
	}

	// Target Page
	public function getTargetPage() {
		return view('page.target');
	}

	// Ep Page
	public function getEpPage() {
		return view('page.ep');
	}

	// Dp Page
	public function getDpPage() {
		return view('page.dp');
	}

	// Te Page
	public function getTePage() {
		return view('page.te');
	}

	// Utility Page
	public function getUtilityPage() {
		return view('page.utility');
	}

	// Process Page
	public function getProcessPage() {
		return view('page.process');
	}

	// Components Page
	public function getComponentsPage() {
		return view('page.components');
	}

	// Ec Page
	public function getEcPage() {
		return view('page.ec');
	}

	// Ee Page
	public function getEePage() {
		return view('page.ee');
	}

	// Er Page
	public function getErPage() {
		return view('page.er');
	}

	// Legal Pages
	// Contact Page
	public function getContactPage() {
		return view('page.contact');
	}

	// Cookie Page
	public function getCookiesPage() {
		return view('page.cookie');
	}

	// Privacy Page
	public function getPrivacyPage() {
		return view('page.privacy');
	}

	// Legal Page
	public function getLegalPage() {
		return view('page.legal');
	}

	// Creative Page
	public function getCreativePage() {
		return view('page.creative');
	}

	// Teachers
	public function getTeachers() {
		$data = (array) $this->functions->getTeachers();
		return view('teacher.index', $data);
	}

	public function getTeacher($path) {
		$data = $this->functions->getTeacher($path);
		// If exist profile
		if (!empty($data->profile)) {
			// If wrong Url
			if ($data->profile->path == $path) {
				// Fill data
				$data = (array) $data;
				// Return view
				return view('teacher.show', $data);
			} else {
				// Redirect if url is wrong
				return Redirect::to($data->profile->url, 301);
			}
		} else {
			return $this->show404();
		}
	}

}
