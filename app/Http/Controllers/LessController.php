<?php

namespace App\Http\Controllers;
use Less;
use File;
use Illuminate\Http\Request;
use Illuminate\Contracts\Config\Repository as Config;

class LessController extends Controller {

	public function compile($fichero) {
		// Obtenemos la configuración de LESS
		$config = Config()->get('less', array());

		// Calculamos la ruta del fichero compilado
		$rutaFichero = $config['public_path'] . '/' . $fichero . '.css';

		// No existe el fichero .less
		if (!is_file($config['less_path'] . '/' . $fichero . '.less')) {
			// Mostramos 404
			abort(404);
		}

		/* Creamos el array de opciones */
		$opciones = array();
		// Estamos en local
		if (env('APP_ENV') === 'local') {
			$opciones['compress'] = false;
			// Estamos en producción
		} else {
			$opciones['compress'] = true;
		}


		// No existe el fichero compilado
		if (!is_file($rutaFichero)) {
			// Nos aseguramos de que la carpeta de archivos compilados exista
			if (!is_dir(dirname($rutaFichero)))
				mkdir(dirname($rutaFichero) . '/', 0777, true);

			// Compilamos por primera vez
			Less::compile($fichero, $opciones);
			// Ya existe el fichero compilado
		} else {
			// Recompilamos
			Less::recompile($fichero, $config['compile_frequency'], $opciones);
		}

		// Devolemos el contenido del fichero
		return response()->make(File::get($rutaFichero))->header('Content-Type', 'text/css');
	}

}
