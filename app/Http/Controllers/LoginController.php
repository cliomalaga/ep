<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class LoginController extends Controller {

	// Login Page
	public function index() {
		return view('login.index');
	}

	public function login(Request $request) {
		$post = $request->all();
		$response = $this->functions->login($post);

		if (!empty($response->user)) {
			$user = $response->user;
			Session::put('user', $user);
		} else {
			Session::flash('login-response', $response->message);
		}
		return redirect('bienvenido');
	}

	public function welcome() {
		if ($this->canShowUrl('bienvenido')) {
			return view('login.welcome');
		} else {
			return $this->redirectLogin();
		}
	}

	public function logout() {
		Session::forget('user');
		return redirect('/');
	}

}
