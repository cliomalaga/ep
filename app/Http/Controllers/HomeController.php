<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller {

	public function index() {
		$data['items'] = $this->functions->getItemsHome();
		$data['faqs'] = $this->functions->getFaqsHome();
		
		return view('home.index', $data);
	}

}
