<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller {

	public function getCategory($category) {
		if ($this->canShowUrl($category)) {
			$data = (array) $this->functions->getCategory($category);
			if (!empty($data)) {
				$data['isArchive'] = false;
				return view('category.index', $data);
			} else {
				return $this->show404();
			}
		} else {
			return $this->redirectLogin();
		}
	}

	public function getSubcategory($category, $subcategory) {
		if ($this->canShowUrl($category)) {
			$data = (array) $this->functions->getSubcategory($category, $subcategory);
			if (!empty($data)) {
				$data['isArchive'] = false;
				return view('category.index', $data);
			} else {
				return $this->show404();
			}
		} else {
			return $this->redirectLogin();
		}
	}

	public function getCategoryArchive($category) {
		$data = (array) $this->functions->getCategoryArchive($category);
		if (!empty($data)) {
			$data['isArchive'] = true;
			return view('category.index', $data);
		} else {
			return $this->show404();
		}
	}

	public function getSubcategoryArchive($category, $subcategory) {
		$data = (array) $this->functions->getSubcategoryArchive($category, $subcategory);
		if (!empty($data)) {
			$data['isArchive'] = true;
			return view('category.index', $data);
		} else {
			return $this->show404();
		}
	}

}
