jQuery(document).ready(
	function ($) {
		bSelectorState = Cookies.get('mipthemeStyleSelector') ? Cookies.get('mipthemeStyleSelector') : !0;
		if (bSelectorState == 'true') {
			$("#style-selector").animate({right: "0"}, 500, function () {});
		}
		$('#style-selector .style-toggle').on('click', function (e) {
			e.preventDefault();
			if (bSelectorState == 'true') {
				$("#style-selector").animate({right: "-295px"}, 500, function () {});
				bSelectorState = 'false';
				Cookies.set('mipthemeStyleSelector', 'false');
			} else {
				$("#style-selector").animate({right: "0"}, 500, function () {});
				bSelectorState = 'true';
				Cookies.set('mipthemeStyleSelector', 'true');
			}
		});
		$('.demo-sites a img').on({
			mousemove: function (e) {
				$(this).next('img').css({right: 295, top: e.pageY - 260});
			}, mouseenter: function () {
				var big = $('<img />', {'class': 'popupimg', src: $(this).attr('data-img')});
				$(this).after(big);
			}, mouseleave: function () {
				$('.popupimg').remove();
			}});
	});