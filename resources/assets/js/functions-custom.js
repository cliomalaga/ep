/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 (function() {
 	//
	// Reload first time
	//
	if (typeof(Storage) !== 'undefined') {
		if (localStorage.getItem('reloaded') === null) {
	        //Reload the page
	        localStorage.setItem('reloaded', true);
        	location.reload(true);
		}else{
			localStorage.removeItem('reloaded');
		}
	  
	}
})();



jQuery(document).ready(function () {
	//
	//	Faqs Block Paginated
	//
	var pageFaqs = 1;
	var minPageFaqs = 1;
	var maxPageFaqs = 4;
	jQuery('.mip-ajax-nav a.prev').click(function (e) {
		if (pageFaqs > minPageFaqs) {
			pageFaqs--;
			if (pageFaqs > minPageFaqs) {
				jQuery('.mip-ajax-nav a.next').removeClass('disabled');
			} else {
				jQuery('.mip-ajax-nav a.prev').addClass('disabled');

			}
			jQuery('#block-faqs').attr('data-page', pageFaqs);
		} else {
			jQuery('.mip-ajax-nav a.prev').addClass('disabled');
		}
	});
	jQuery('.mip-ajax-nav a.next').click(function (e) {
		if (pageFaqs < maxPageFaqs) {
			pageFaqs++;
			if (pageFaqs < maxPageFaqs) {
				jQuery('.mip-ajax-nav a.prev').removeClass('disabled');
			} else {
				jQuery('.mip-ajax-nav a.next').addClass('disabled');
			}
			jQuery('#block-faqs').attr('data-page', pageFaqs);
		} else {
			jQuery('.mip-ajax-nav a.next').addClass('disabled');
		}
	});







	//
	//	Isotope filter string
	//
// quick search regex
	var qsRegex;

// init Isotope
	var jQuerygrid = jQuery('.grid').isotope({
		itemSelector: '.element-item',
		layoutMode: 'fitRows',
		filter: function () {
			return qsRegex ? jQuery(this).text().match(qsRegex) : true;
		},
		sortBy: {
			time: function ($elem) {
				return $elem.find('.date').attr('datetime');
			}
		},
		getSortData: {
			fileby: '.file-by',
			filedate: '.file-date',
		},
		sortAscending: true
	});

// use value of search field to filter
	var jQueryquicksearch = jQuery('.quicksearch').keyup(debounce(function () {
		qsRegex = new RegExp(jQueryquicksearch.val(), 'gi');
		jQuerygrid.isotope();
	}, 200));

// debounce so filtering doesn't happen every millisecond
	function debounce(fn, threshold) {
		var timeout;
		return function debounced() {
			if (timeout) {
				clearTimeout(timeout);
			}
			function delayed() {
				fn();
				timeout = null;
			}
			timeout = setTimeout(delayed, threshold || 100);
		}
	}


	// bind sort button click
	jQuery('.sort-button-group').on('click', 'button', function () {

		/* Get the element name to sort */
		var sortValue = jQuery(this).attr('data-sort-value');
		/* Get the sorting direction: asc||desc */
		var direction = jQuery(this).attr('data-sort-direction');
		/* convert it to a boolean */
		var isAscending = (direction == 'asc');
		var newDirection = (isAscending) ? 'desc' : 'asc';
		var span = jQuery(this).find('.glyphicon');
		span.toggleClass('glyphicon-chevron-up glyphicon-chevron-down');

		/* pass it to isotope */
		jQuerygrid.isotope({sortBy: sortValue, sortAscending: isAscending});

		jQuery(this).attr('data-sort-direction', newDirection);

	});

	// Show/Fade Buton to top
	jQuery(window).on("mousewheel DOMMouseScroll scroll onscroll touchmove", function (e) {
		if (jQuery(this).scrollTop() > 100) {
			jQuery('#btn-go-top').fadeIn();
		} else {
			jQuery('#btn-go-top').fadeOut();
		}
	});
	// Go to top
	jQuery("#btn-go-top").click(function () {
		jQuery("html, body").animate({scrollTop: 0}, "slow");
	});
});

function initMap() {
	var centermarker = {lat: 36.718, lng: -4.477};
	var elementExists = document.getElementById('map-contact');
	if (elementExists) {
		var map = new google.maps.Map(document.getElementById('map-contact'), {
			zoom: 15,
			center: centermarker,
			disableDefaultUI: true
		});
		var icon = {
			url: 'http://www.free-icons-download.net/images/blue-map-marker-icons-32280.png',
			scaledSize: new google.maps.Size(70, 70), // scaled size
			origin: new google.maps.Point(0, 0), // origin
			anchor: new google.maps.Point(20, 50) // anchor
		};
		var marker = new google.maps.Marker({
			position: centermarker,
			icon: icon,
			map: map
		});
	}
}

