@extends('template.master')

@section('title')
| Página no encontrada
@endsection

@section('sidebar')
@include('404.sidebar-404')
@endsection


@section('content')
<article id="post-297" class="article-post clearfix post-297 post type-post status-publish format-image has-post-thumbnail hentry category-pc category-ps4 category-wii-u category-xbox-one post_format-post-format-image" itemscope="" itemtype="http://schema.org/Article">


	<div class="container-404">
		<h1>404</h1>
		<h3>PÁGINA NO ENCONTRADA</h3>
		<h5>Asegúrese que la dirección existe y que está escrita correctamente, o utilice nuestro <a href="{{ url('buscar') }}">buscador</a>. Si necesita ayuda, por favor <a href="{{ url('contacto') }}">contacte</a> con nosotros y le responderemos tán pronto como nos sea posible.</h5>
		<img itemprop="image" src="{{resources('images/ep/404.png')}}" class="img-responsive">
	</div>




	<!-- Other users viewed -->
	<aside id="others-viewed" class="posts-related loop-cat standard loop-cat-3">
		@include('components.others-viewed')
	</aside>

	<br>
	<br>


</article>
@endsection