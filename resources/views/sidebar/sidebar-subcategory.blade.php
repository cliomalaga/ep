@inject('functions', 'App\Repositories\Functions\FunctionsRepository')


@php
$data['id_category']=$item->category->id;
$data['id_subcategory']=$item->subcategory->id;
$sameSubcategory = (array) $functions->getSameSubcategory($data);
@endphp



@if(!empty($item) && count($sameSubcategory)>1)
<!-- No show if only exist one item on same subcategory, because is same that item -->
<aside class="widget module-news sidebar-category">
	<header>
		<div class="title">
			<span>{{ $item->subcategory->name }}</span>
		</div>
	</header>
	<div class="article-container">
		@foreach($sameSubcategory as $same)
		@if($item->id != $same->id)
		<article class="def def-small block-archive">
			<div class="row clearfix">
				<div class="col-xs-4">
					<figure class="overlay relative">
						<a itemprop="url" href="{{ url($same->url) }}" class="thumb-overlay-small">
							<img itemprop="image" src="{{urlDashboard($same->image)}}" alt="{{ ucfirst($same->title) }}" class="img-responsive">
						</a>
						<figcaption>
							<div class="entry-meta"></div>
						</figcaption>
					</figure>

				</div>
				<div class="col-xs-8 no-left">
					<h3 itemprop="name">
						<a itemprop="url" href="{{ url($same->url) }}">
							{{ ucfirst(str_limit($same->title, $limit = 50, $end = '...')) }}
						</a>
					</h3>
					<div class="entry-meta">
						<time class="entry-date" datetime="{{ $same->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($same->created_at)) }}</time>
					</div>
				</div>
			</div>
		</article>
		@endif
		@endforeach
	</div>
</aside>
@endif