@inject('functions', 'App\Repositories\Functions\FunctionsRepository')

@php
$data['number']=5;
$timeline = $functions->getTimeline($data);
@endphp


@if(!empty($timeline))
<aside class="widget module-timeline">
	<header>
		<div class="title">
			<span>TIMELINE</span>
		</div>
	</header>
	<div class="articles">
		@foreach($timeline as $item)
		<article class="def">
			<div class="cnt">
				<i class="bullet parent-bullet-3 bullet-3"></i>
				<span class="category parent-cat-1 cat-1">
					<a href="{{ url($item->category->url) }}">
						{{ ucwords(mb_strtolower($item->category->name)) }}
					</a>
					-
					<a href="{{ url($item->subcategory->url) }}">
						{{ ucwords(mb_strtolower($item->subcategory->name)) }}
					</a>
				</span>
				<h3>
					<span class="">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</span><br>
					<a href="{{ url($item->url) }}">
						{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
					</a>

				</h3>
			</div>
		</article>
		@endforeach
	</div>
</aside>
@endif