@inject('functions', 'App\Repositories\Functions\FunctionsRepository')

@php
$data['number']=6;
$archive = $functions->getArchive($data);
@endphp


@if(!empty($archive))
<aside class="widget module-news">
	<header>
		<div class="title">
			<span>BIBLIOTECA</span>
		</div>
	</header>
	<div class="article-container">
		@foreach($archive as $item)
		<article class="def def-small block-archive">
			<div class="row clearfix">
				<div class="col-xs-5">
					<figure class="overlay relative">
						<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
							<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
						</a>
						<figcaption>
							<div class="entry-meta"></div>
						</figcaption>
					</figure>

				</div>
				<div class="col-xs-7 col-sm-6 no-left">
					<span class="entry-category parent-cat-8 cat-8">
						<a itemprop="url" href="{{ url($item->category->url) }}">
							{{ ucwords(mb_strtolower($item->category->name)) }}
						</a>
					</span>
					<h3 itemprop="name">
						<a itemprop="url" href="{{ url($item->url) }}">
							{{ ucfirst(str_limit($item->title, $limit = 50, $end = '...')) }}
						</a>
					</h3>
					<div class="entry-meta">
						<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">
							{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}
						</time>
					</div>
				</div>
			</div>
		</article>
		@endforeach
	</div>
</aside>
@endif