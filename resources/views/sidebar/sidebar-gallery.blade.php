@inject('functions', 'App\Repositories\Functions\FunctionsRepository')

@php
$gallery = $functions->getGallery();
@endphp


@if(!empty($gallery))
<aside class="widget module-photos">
	<header>
		<div class="title">
			<span>GALERÍA</span>
		</div>
	</header>
	<div id="weekly-gallery" class="gallery-sidebar weekly-gallery article-container">
		@if(!empty($gallery[0]))
		<article class="relative clearfix">
			<a href="{{urlDashboard($gallery[0]->image)}}" title="{{ ucfirst($gallery[0]->title) }}">
				<img src="{{urlDashboard($gallery[0]->image)}}" alt="{{ ucfirst($gallery[0]->title) }}" class="img-responsive">
				<div class="zoomix">
					<i class="fa fa-search"></i>
				</div>
			</a>
		</article>
		@endif
		<div class="row">
			<div class="col-xs-6">

				<article class="clearfix">
					@if(!empty($gallery[1]))
					<a href="{{urlDashboard($gallery[1]->image)}}" title="{{ ucfirst($gallery[1]->title) }}">
						<img src="{{urlDashboard($gallery[1]->image)}}" alt="{{ ucfirst($gallery[1]->title) }}" class="img-responsive">
						<div class="zoomix">
							<i class="fa fa-search"></i>
						</div>
					</a>
					@endif
				</article>
			</div>
			<div class="col-xs-6">
				<article class="clearfix">
					@if(!empty($gallery[2]))
					<a href="{{urlDashboard($gallery[2]->image)}}" title="{{ ucfirst($gallery[2]->title) }}">
						<img src="{{urlDashboard($gallery[2]->image)}}" alt="{{ ucfirst($gallery[2]->title) }}" class="img-responsive">
						<div class="zoomix">
							<i class="fa fa-search"></i>
						</div>
					</a>
					@endif
				</article>
			</div>
			@php
			$limitGallery=count($gallery);
			@endphp
			@for($i=3;$i<$limitGallery;$i++)
			<div class="col-xs-6 hidden">
				<article class="clearfix">
					@if(!empty($gallery[$i]))
					<a href="{{urlDashboard($gallery[$i]->image)}}" title="{{ ucfirst($gallery[$i]->title) }}">
						<img src="{{urlDashboard($gallery[$i]->image)}}"  alt="{{ ucfirst($gallery[$i]->title) }}" class="img-responsive">
						<div class="zoomix">
							<i class="fa fa-search"></i>
						</div>
					</a>
					@endif
				</article>
			</div>
			@endfor
		</div>
	</div>
</aside>
@endif