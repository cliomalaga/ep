@inject('functions', 'App\Repositories\Functions\FunctionsRepository')

@php
$last = $functions->getLast()[0];
@endphp


@if(!empty($last))
<aside class="widget module-reviews">
	<header>
		<div class="title">
			<span>LO MÁS NUEVO</span>
		</div>
	</header>
	<div class="article-container">
		<article class="def def-medium def-overlay item-count-0">
			<figure class="overlay relative">
				<a itemprop="url" href="{{ url($last->url) }}" class="thumb-overlay">
					<img itemprop="image" src="{{urlDashboard($last->image)}}" alt="{{ ucfirst($last->title) }}">
				</a>
				<figcaption>
					<div class="entry">
						<h2 itemprop="name">
							<a itemprop="url" href="{{ url($last->url) }}">
								{{ ucfirst(str_limit($last->title, $limit = 100, $end = '...')) }}
							</a>
						</h2>
						<div class="post-meta text-center">
							<time class="entry-date" datetime="{{ $last->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($last->created_at)) }}</time>
							<a itemprop="url" href="{{ url($last->category->url) }}">{{ ucwords(mb_strtolower($last->category->name)) }}</a>
						</div>
					</div>
				</figcaption>
			</figure>

		</article>
	</div>
</aside>
@endif