@extends('template.master')

@section('title')
| Profesores
@endsection

@section('sidebar')
@include('teacher.sidebar-teacher')
@endsection



@section('content')
<div class="content-container">

	<article id="post-297" class="article-post clearfix post-297 post type-post status-publish format-image has-post-thumbnail hentry category-pc category-ps4 category-wii-u category-xbox-one post_format-post-format-image" itemscope="" itemtype="http://schema.org/Article">
		<!--Item-->
		<header>
			<div class="breadcrumb">
				<span>

				</span>
				<div class="vbreadcrumb" typeof="v:Breadcrumb">
					<a href="{{ url('/') }}" rel="v:url" property="v:title" class="home">
						Home
					</a>
				</div>
				<div class="vbreadcrumb" typeof="v:Breadcrumb">
					<a title="Profesores" rel="v:url" property="v:title">
						Profesores
					</a>
				</div>
			</div>
			<h2 class="h2-categories">
				<span>
					Profesores
				</span>
			</h2>
		</header>



		<section id="author-team-page">
			@foreach($profiles as $profile)
			<div class="author-box">
				<a href="{{ url($profile->url) }}">
					<img src="{{ urlDashboard($profile->avatar) }}" alt="{{ ucwords(mb_strtolower($profile->name)) }} {{ ucwords(mb_strtolower($profile->lastname)) }}" class="avatar avatar-115 wp-user-avatar wp-user-avatar-115 alignnone photo">
				</a>
				<p class="name">
					<a href="{{ url($profile->url) }}">
						{{ ucwords(mb_strtolower($profile->name)) }} {{ ucwords(mb_strtolower($profile->lastname)) }}
					</a>
				</p>
				<div class="author-meta"></div>
				<p class="desc">
					{{$profile->about }}
				</p>
				<p class="follow">
					<!--			<a class="home" href="http://#">
									<i class="fa fa-home"></i>
								</a>-->
					@if(!empty($profile->Facebook))
					<a class="facebook" target="_blank" href="{{ $profile->Facebook }}">
						<i class="fa fa-facebook"></i>
					</a>
					@endif
					@if(!empty($profile->Twitter))
					<a class="twitter" target="_blank" href="{{ $profile->Twitter }}">
						<i class="fa fa-twitter"></i>
					</a>
					@endif
					@if(!empty($profile->Linkedin))
					<a class="linkedin" target="_blank" href="{{ $profile->Linkedin }}">
						<i class="fa fa-linkedin"></i>
					</a>
					@endif
					@if(!empty($profile->Google))
					<a class="google-plus" target="_blank" href="{{ $profile->Google }}">
						<i class="fa fa-google-plus"></i>
					</a>
					@endif
				</p>
			</div>
			@endforeach
		</section>






		<br>
		<br>


	</article>
</div>
@endsection