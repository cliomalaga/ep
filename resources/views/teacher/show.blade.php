@extends('template.master')

@section('title')
| {{ $profile->name }} {{ $profile->lastname }}
@endsection

@section('sidebar')
@include('teacher.sidebar-teacher')
@endsection



@section('content')
<div class="content-container">

	<article id="post-297" class="article-post clearfix post-297 post type-post status-publish format-image has-post-thumbnail hentry category-pc category-ps4 category-wii-u category-xbox-one post_format-post-format-image" itemscope="" itemtype="http://schema.org/Article">
		<!--Item-->
		<header>
			<div class="breadcrumb">
				<span>

				</span>
				<div class="vbreadcrumb" typeof="v:Breadcrumb">
					<a href="{{ url('/') }}" rel="v:url" property="v:title" class="home">
						Home
					</a>
				</div>
				<div class="vbreadcrumb" typeof="v:Breadcrumb">
					<a href="{{ url('profesores') }}" title="Profesores" rel="v:url" property="v:title">
						Profesores
					</a>
				</div>
				<div class="vbreadcrumb" typeof="v:Breadcrumb">
					<a title="{{ $profile->name }} {{ $profile->lastname }}" rel="v:url" property="v:title">
						{{ ucwords(mb_strtolower($profile->name)) }} {{ ucwords(mb_strtolower($profile->lastname)) }}
					</a>
				</div>
			</div>
		</header>

		<!--Author-->
		<section id="author-team-page">
			<div class="author-box">
				<a href="{{ url($profile->url) }}">
					<img src="{{ urlDashboard($profile->avatar) }}" alt="{{ ucwords(mb_strtolower($profile->name)) }} {{ ucwords(mb_strtolower($profile->lastname)) }}" class="avatar avatar-115 wp-user-avatar wp-user-avatar-115 alignnone photo">
				</a>
				<p class="name">
					<a href="{{ url($profile->url) }}">
						{{ ucwords(mb_strtolower($profile->name)) }} {{ ucwords(mb_strtolower($profile->lastname)) }}
					</a>
				</p>
				<div class="author-meta"></div>
				<p class="desc">
					{{ ucfirst($profile->about) }}
				</p>
				<p class="follow">
					<!--			<a class="home" href="http://#">
									<i class="fa fa-home"></i>
								</a>-->
					@if(!empty($profile->Facebook))
					<a class="facebook" target="_blank" href="{{ $profile->Facebook }}">
						<i class="fa fa-facebook"></i>
					</a>
					@endif
					@if(!empty($profile->Twitter))
					<a class="twitter" target="_blank" href="{{ $profile->Twitter }}">
						<i class="fa fa-twitter"></i>
					</a>
					@endif
					@if(!empty($profile->Linkedin))
					<a class="linkedin" target="_blank" href="{{ $profile->Linkedin }}">
						<i class="fa fa-linkedin"></i>
					</a>
					@endif
					@if(!empty($profile->Google))
					<a class="google-plus" target="_blank" href="{{ $profile->Google }}">
						<i class="fa fa-google-plus"></i>
					</a>
					@endif
				</p>
			</div>
		</section>



		<!--Author Articles-->
		<aside id="related-posts" class="posts-related loop-cat standard loop-cat-3">
			<header>
				<h2>
					<span>Artículos del autor</span>
				</h2>
			</header>
			<div class="cat-layout related-items author-items">
				<div class="row">
					@if(!empty($items))
					@foreach($items as $item)
					<div class="col-sm-4">
						<article class="def def-medium">
							<figure class="overlay relative">
								<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
									<img itemprop="image" src="{{urlDashboard($item->image)}}" width="277" height="190" alt="{{ ucfirst($item->title) }}" class="img-responsive">
								</a>
								<figcaption>
									<div class="entry-meta"></div>
								</figcaption>
							</figure>
							<div class="entry">
								<span class="entry-category parent-cat-1 cat-1">
									<a itemprop="url" href="{{ url($item->category->url) }}">
										{{ ucwords(mb_strtolower($item->category->name))  }}
									</a>
									-
									<a itemprop="url" href="{{ url($item->subcategory->url) }}">
										{{ ucwords(mb_strtolower($item->subcategory->name)) }}
									</a>
								</span>
								<h3 itemprop="name">
									<a itemprop="url" href="{{ url($item->url) }}">
										{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
									</a>
								</h3>
								<div class="entry-meta">
									<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">
										{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}
									</time>
								</div>
							</div>
						</article>
					</div>
					@endforeach
					@else
					<h1>Este profesor no ha publicado artículos todavía.</h1>
					@endif
				</div>
			</div>
		</aside>


		<br>
		<br>


	</article>
</div>
@endsection