@extends('template.master')

@section('title')
| Login
@endsection



@section('content')
<div class="content-container">
	<header>
		<div class="breadcrumb">
			<span>

			</span>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a href="{{ url('/') }}" rel="v:url" property="v:title" class="home">
					Home
				</a>
			</div>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a title="Login" rel="v:url" property="v:title">
					Login
				</a>
			</div>
		</div>

		<h2 class="h2-categories">
			<span>
				Login
			</span>
		</h2>
	</header>



	<div class="cat-layout clearfix page-contact page-static">
		<div class="content-contact">
			<div class="row">
				<div class="col-md-12">
					<p>
						Para acceder a contenido exclusivo y secciones restringidas introduzca su usuario y clave en el siguiente formulario.
						<br>
						Si quiere tener acceso, pongase en <a href="{{ url('contacto')}}">contacto</a> con nosotros y le explicaremos los requisitos previos para poder acceder a estas zonas restringidas de la web.
					</p>
					<div id="form-contact">
						<br>
						@include('components.form-login')
						<br>
						<br>
						<br>
						<br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection