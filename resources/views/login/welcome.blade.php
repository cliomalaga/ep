@extends('template.master')

@section('title')
| Bienvenido
@endsection



@section('content')
<div class="content-container">
	<header>
		<div class="breadcrumb">
			<span>

			</span>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a href="{{ url('/') }}" rel="v:url" property="v:title" class="home">
					Home
				</a>
			</div>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a title="Bienvenido" rel="v:url" property="v:title">
					Bienvenido
				</a>
			</div>
		</div>

		<h2 class="h2-categories">
			<span>
				Bienvenido
			</span>
		</h2>
	</header>



	<div class="cat-layout clearfix page-static ">
		<div class="content-contact">
			<div class="row">
				<div class="col-md-12">
					<p>
						Bienvenido a la zona exclusiva de Evaluación Psicológica, ahora podrá tener acceso a la sección <a href="{{ url('documentos') }}">DOCUMENTOS</a>. Próximamente presentaremos nuevas ventajas de tener un usuario en nuestra web.
					</p>


					<div class="back-home">
						<a href="{{ url('/') }}">
							Ir al Inicio
						</a>
					</div>
					
					<br>
					<br>
					<br>
					<br>
					<br>
				</div>
			</div>
		</div>
	</div>

	@endsection