@inject('functions', 'App\Repositories\Functions\FunctionsRepository')

@php
$menu = $functions->getMenu();
@endphp

@if(!empty($menu))
<nav id="mobile-menu" class="mm-menu mm-horizontal mm-offcanvas mm-front">
	<a class="logo-menu-left" itemprop="url" href="{{ url('/') }}">
		<img src="{{resources('images/ep/LogoFullWhite-NotBook.png')}}" alt="Evaluación Psicológica">
	</a>


	<form id="search-form-mobile" class="mm-search"  method="POST" action="buscador">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="text" id="input-search-menu" name="search" placeholder="Buscar ..." value="">
		<button id="button-search-menu">
			<span class="glyphicon glyphicon-search"></span>
		</button>
	</form>


	<ul id="menu-mobile" class="nav clearfix mm-list mm-panel mm-opened mm-current">
		<li id="mobile-nav-menu-item-553" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="{{ url('/') }}">Home</a>
		</li>
		<li id="mobile-nav-menu-item-408" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-13 ">
			<a class="mm-subopen" href="#menu-mobile-0"></a>
			<a href="#menu-mobile-0">Nosotros</a>
		</li>
		<li id="mobile-nav-menu-item-409" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-13 ">
			<a class="mm-subopen" href="#menu-mobile-1"></a>
			<a href="#menu-mobile-1">Pruebas</a>
		</li>
		<li id="mobile-nav-menu-item-411" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-4 ">
			<a class="mm-subopen" href="#menu-mobile-2"></a>
			<a href="#menu-mobile-2">Preguntas</a>
		</li>
		<li id="mobile-nav-menu-item-422" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-7 ">
			<a class="mm-subopen" href="#menu-mobile-3"></a>
			<a href="#menu-mobile-3">Documentos</a>
		</li>
		<li id="mobile-nav-menu-item-410" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-13 ">
			<a class="mm-subopen" href="#menu-mobile-4"></a>
			<a href="#menu-mobile-4">Enseñanzas</a>
		</li>
		<li id="mobile-nav-menu-item-411" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-13 ">
			<a class="mm-subopen" href="#menu-mobile-5"></a>
			<a href="#menu-mobile-5">Casos</a>
		</li>
		<li id="mobile-nav-menu-item-411" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-13 ">
			<a class="mm-subopen" href="#menu-mobile-5"></a>
			<a href="#menu-mobile-5">Casos</a>
		</li>
		<li id="mobile-nav-menu-item-412" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-13 ">
			<a class="mm-subopen" href="#menu-mobile-6"></a>
			<a href="#menu-mobile-6">Investigaciones</a>
		</li>
	</ul>


	<!--NOSOTROS-->
	<ul class="sub-menu mm-list mm-panel mm-hidden" id="menu-mobile-0">
		<li class="mm-subtitle">
			<a class="mm-subclose" href="#menu-mobile">Nosotros</a>
		</li>
		<li id="mobile-nav-menu-item-542" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="{{ url('quienes-somos') }}">Quienes somos</a>
		</li>
		<li id="mobile-nav-menu-item-543" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="{{ url('admision-socios') }}">Admision Socios</a>
		</li>
		<li id="mobile-nav-menu-item-543" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="{{ url('cuota-socios') }}">Cuota Socios</a>
		</li>
		<li id="mobile-nav-menu-item-543" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="{{ url('contacto') }}">Contacto</a>
		</li>
		<!--
		<li id="mobile-nav-menu-item-542" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="{{ url('miembros') }}">Miembros</a>
		</li>
		-->
		<!--	
		<li id="mobile-nav-menu-item-541" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="{{ url('objetivos') }}">Objetivos</a>
		</li>
		-->
		<li id="mobile-nav-menu-item-540" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="{{ url('creative-commons') }}">Creative Commmons</a>
		</li>
	</ul>






	@php
	$section = 'Pruebas'
	@endphp
	<ul class="sub-menu mm-list mm-panel mm-hidden" id="menu-mobile-1">
		<li class="mm-subtitle">
			<a class="mm-subclose" href="#menu-mobile">{{ $section }}</a>
		</li>
		@foreach($menu->submenu1 as $item)
		<li id="mobile-nav-menu-item-543" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="{{ url('/'.str_slug($section).'/'.str_slug( $item->name)) }}">{{ $item->name }}</a>
		</li>
		@endforeach
	</ul>

	@php
	$section = 'Preguntas'
	@endphp
	<ul class="sub-menu mm-list mm-panel mm-hidden" id="menu-mobile-2">
		<li class="mm-subtitle">
			<a class="mm-subclose" href="#menu-mobile">{{ $section }}</a>
		</li>
		@foreach($menu->submenu2 as $item)
		<li id="mobile-nav-menu-item-543" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="{{ url('/'.str_slug($section).'/'.str_slug( $item->name)) }}">{{ $item->name }}</a>
		</li>
		@endforeach
	</ul>


	@php
	$section = 'Documentos'
	@endphp
	<ul class="sub-menu mm-list mm-panel mm-hidden" id="menu-mobile-3">
		<li class="mm-subtitle">
			<a class="mm-subclose" href="#menu-mobile">{{ $section }}</a>
		</li>
		@foreach($menu->submenu3 as $item)
		<li id="mobile-nav-menu-item-543" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="{{ url('/'.str_slug($section).'/'.str_slug( $item->name)) }}">{{ $item->name }}</a>
		</li>
		@endforeach
	</ul>

	@php
	$section = 'Enseñanzas'
	@endphp
	<ul class="sub-menu mm-list mm-panel mm-hidden" id="menu-mobile-4">
		<li class="mm-subtitle">
			<a class="mm-subclose" href="#menu-mobile">{{ $section }}</a>
		</li>
		@foreach($menu->submenu4 as $item)
		<li id="mobile-nav-menu-item-543" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="{{ url('/'.str_slug($section).'/'.str_slug( $item->name)) }}">{{ $item->name }}</a>
		</li>
		@endforeach
	</ul>

	@php
	$section = 'Casos'
	@endphp
	<ul class="sub-menu mm-list mm-panel mm-hidden" id="menu-mobile-5">
		<li class="mm-subtitle">
			<a class="mm-subclose" href="#menu-mobile">{{ $section }}</a>
		</li>
		@foreach($menu->submenu5 as $item)
		<li id="mobile-nav-menu-item-543" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="{{ url('/'.str_slug($section).'/'.str_slug( $item->name)) }}">{{ $item->name }}</a>
		</li>
		@endforeach
	</ul>

	@php
	$section = 'Investigaciones'
	@endphp
	<ul class="sub-menu mm-list mm-panel mm-hidden" id="menu-mobile-6">
		<li class="mm-subtitle">
			<a class="mm-subclose" href="#menu-mobile">{{ $section }}</a>
		</li>
		@foreach($menu->submenu6 as $item)
		<li id="mobile-nav-menu-item-543" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="{{ url('/'.str_slug($section).'/'.str_slug( $item->name)) }}">{{ $item->name }}</a>
		</li>
		@endforeach
	</ul>
</nav>
@endif