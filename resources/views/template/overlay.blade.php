<div id="pbOverlay">
	<input type="checkbox" id="pbThumbsToggler" checked="" hidden="">
	<div class="pbLoader">
		<b></b>
		<b></b>
		<b></b>
	</div>
	<div id="pbPrevBtn" class="prevNext">
		<b></b>
	</div>
	<div id="pbNextBtn" class="prevNext">
		<b></b>
	</div>
	<div class="pbWrapper">
		<img>
		<div></div>
	</div>
	<div id="pbCloseBtn"></div>
	<div id="pbAutoplayBtn">
		<div class="pbProgress"></div>

	</div>
	<div id="pbCaption">
		<label for="pbThumbsToggler" title="thumbnails on/off"></label>
		<div class="pbCaptionText">
			<div class="title"></div>
			<div class="counter"></div>

		</div>
		<div class="pbThumbs">

		</div>

	</div>

</div>