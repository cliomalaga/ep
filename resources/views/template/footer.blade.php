<section id="page-footer">
	<section id="footer-section-top" class="footer-section-top-2">
		<div class="container">
			<div class="row">
				<div class="col col-sm-6 text-center">
					<aside id="text-13" class="widget widget_text clearfix">
						<header>
							<div class="title">
								<span>Evaluación Psicológica</span>
							</div>
						</header>
						<div class="textwidget">
							Sitio en internet sobre evaluación y diagnóstico psicológico en español, dirigido a profesionales y profesores de la evaluación psicológica.
						</div>
					</aside>
				</div>
				<div class="col col-sm-6 text-center">
					<aside id="text-14" class="widget widget_text clearfix">
						<header>
							<div class="title">
								<span>¿Quieres contactar con nosotros?</span>
							</div>
						</header>
						<div class="textwidget">
							Haznos cualquier tipo de consulta, sugerencia o aportación a nuestro correo <a href="mailto:info@evaluacionpsicologica.es">info@evaluacionpsicologica.es</a> o utilice nuestro formulario de contacto de la barra lateral de la derecha, ¡le responderemos tan rápido como nos sea posible!
						</div>
					</aside>
				</div>
			</div>
		</div>
	</section>
	<section id="footer-section-bottom" class="footer-section-bottom-1">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 text-center">
					<aside class="widget">
						<div>
							<a href="{{ url('/') }}">
								<img src="{{resources('images/ep/LogoFullWhite.png')}}" alt="Evaluación Psicológica">
								<img class="seaep" src="{{resources('images/ep/seaep.jpg')}}" alt="Sociedad Española para el Avance de la Evaluación Psicológica" title="Sociedad Española para el Avance de la Evaluación Psicológica">
							</a>
						</div>
					</aside>
				</div>
				<div class="col-sm-8 text-center links-footer">
					<div class="row">
						<div class="col-xs-6 text-center">

							<h3>
								Legal
							</h3>
							<ul>
								<li>
									<a href="{{ url('creative-commons') }}" target="_blank">Creative Commons</a>
								</li>
								<li>
									<a href="{{ url('aviso-legal') }}" target="_blank">Aviso Legal</a>
								</li>
								<li>
									<a href="{{ url('politica-privacidad') }}" target="_blank">Política de Privacidad</a>
								</li>
								<li>
									<a href="{{ url('politica-cookies') }}" target="_blank">Política de Cookies</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-6 text-center">
							<h3>
								Otros Enlaces
							</h3>
							<ul>
								<li>
									<a href="http://conducta.org" target="_blank">Contextos</a>
								</li>
								<li>
									<a href="https://www.uma.es/" target="_blank">Universidad de Málaga</a>
								</li>
							</ul>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>



	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					© <a href="{{ url('/') }}">Evaluación Psicológica</a> {{ date('Y') }}. All rights reserved.
				</div>
				<div class="col-sm-6 text-right">
					Designed by <a target="_blank" href="https://es.linkedin.com/in/gabrielvaleroperez">Gabriel Valero</a>
				</div>
			</div>
		</div>
	</div>
</section>