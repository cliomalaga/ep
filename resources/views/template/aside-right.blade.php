<div id="style-selector" style="width: 295px;">
	<style type="text/css" scoped="scoped">
		.ss-button,.ss-title{text-transform:uppercase}
		#style-selector{font-family:Roboto,Arial,sans-serif;position:fixed;right:-295px;top:0;z-index:100000;-webkit-box-shadow:-3px 0 50px -2px rgba(0,0,0,.14);-moz-box-shadow:-3px 0 50px -2px rgba(0,0,0,.14);box-shadow:-3px 0 50px -2px rgba(0,0,0,.14);background:#fff;height:100%;width:280px}
		#style-selector-container{height:100%;overflow-x:hidden;overflow-y:auto;-webkit-transition:all .5s;transition:all .5s}
		#style-selector .style-toggle{width:52px;height:56px;cursor:pointer;opacity:1;background:#fff;-moz-border-radius:5px 0 0 5px;-webkit-border-radius:5px 0 0 5px;border-radius:5px 0 0 5px;-webkit-box-shadow:-3px 0 5px -2px rgba(0,0,0,.14);-moz-box-shadow:-3px 0 5px -2px rgba(0,0,0,.14);box-shadow:-3px 0 5px -2px rgba(0,0,0,.14);float:left;margin-left:-52px;margin-top:150px;top:100px}
		#style-selector .style-toggle:before{color:#000;content:"\f013";font-family:FontAwesome;font-size:23px;font-weight:lighter!important;line-height:56px;text-align:center;padding-left:15px}
		.ss-title{font-size:15px;color:#000;text-align:center;display:block;margin-bottom:15px}
		.ss-button,.ss-desc{text-align:center;font-size:13px;display:inline-block}
		.ss-content{border-bottom:1px solid #ddd;padding-bottom:25px;margin:0 30px 35px}
		.ss-desc{color:#5c5c5c;line-height:20px;margin-bottom:35px}
		.ss-button{width:48%;height:30px;line-height:30px;color:#000;border:1px solid #ddd;-webkit-transition:all .3s;-moz-transition:all .3s;transition:all .3s;-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px}
		.ss-button.active,.ss-button:hover{background:#ddd;color:#000}
		.wide-button{margin-right:2%}
		.buy-button{width:100%;height:45px;line-height:45px;font-size:15px;border:none;color:#fff;background-color:#f1a600;text-decoration:none}
		.buy-button:hover{color:#fff;background-color:#ed5565}
		#style-selector .demo-sites a{display:block;margin-bottom:1px}
		#style-selector .demo-sites .popupimg{position:absolute;z-index:1000000}
		.ss-no-styles{padding-bottom:0;margin-bottom:0;border:none}
		.ss-content-first{padding-top:30px;padding-bottom:0;border:none}
		.demos-wrapper{margin-left:0;margin-right:0;margin-bottom:0;border:none}
		.ss-content-last{padding-bottom:30px}
		#style-selector .ss-no-styles .ss-button{width:100%}@media only screen and (max-width:800px){#style-selector{display:none}}
	</style>
	<div class="style-toggle">

	</div>
	<div id="style-selector-container">
		<div class="style-selector-wrapper">
			<div class="">
				<span class="ss-title">Pregúntanos</span>
				<span class="ss-desc">
					Si tienes cualquier consulta, sugerencia o aportación no dudes en hacérnoslo saber. Nos pondremos en contacto con usted, tán brevemente como nos sea posible.
				</span>
			</div>

			<div id="form-aside-right" class="ss-content">
				@include('components.form-contact', ['section'=>'contact-aside'])
			</div>
		</div>
	</div>
</div>