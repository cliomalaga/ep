<header id="page-header-mobile" class="visible-xs visible-sm">
	<div id="mobile-sticky" class="row">
		<div class="col-xs-1">
			<a id="nav-expander" href="#mobile-menu">
				<i class="fa fa-bars"></i>
			</a>
		</div>
		<div class="col-xs-9">
			<div class="logo">
				<a href="{{ url('/') }}">
					<img class="logo-menu-mobile" src="{{resources('images/ep/LogoFullWhite-NotBook.png')}}" alt="Evaluación Psicológica">
				</a>
			</div>
		</div>
		<div class="col-xs-1 text-right">
 
		</div>
	</div>
</header>