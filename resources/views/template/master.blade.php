<!DOCTYPE html>
<!-- saved from url=(0032)http://newsland.newsthemes.info/ -->
<html class="no-js js_active vc_desktop vc_transform vc_transform wf-roboto-n4-active wf-robotoslab-n4-active wf-robotocondensedampsubsetlatin-n4-inactive wf-active" lang="en-US" style="transform: none;">
	<head>
		<!-- Base -->
		<base href="/EP/">


		<!-- Meta -->
		<title>Evaluación Psicológica @yield('title')</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Evaluación Psicológica">
		<meta name="author" content="Evaluación Psicológica">
		<meta name="keywords" content="Evaluación Psicológica"/>


		<!-- Favicon -->
		<link rel="shortcut icon" type="image/ico" href="{{resources('images/ep/favicon.ico')}}" />


		<!-- No Cache -->
		<meta http-equiv="Expires" content="0">
		<meta http-equiv="Last-Modified" content="0">
		<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
		<meta http-equiv="Pragma" content="no-cache">


		<!-- CSS Sheets-->
		<link rel="stylesheet" type="text/css" media="all" href="{{resources('css/template/bootstrap.min.css')}}">
		<link rel="stylesheet" type="text/css" media="all" href="{{resources('css/template/mip.external.css')}}">
		<link rel="stylesheet" type="text/css" media="all" href="{{resources('css/template/style.css')}}">
		<link rel="stylesheet" type="text/css" media="all" href="{{resources('css/template/media-queries.css')}}">
		<link rel="stylesheet" type="text/css" media="all" href="{{resources('css/template/dynamic.css')}}">
		<link rel="stylesheet" type="text/css" media="all" href="{{resources('css/template/typography.css')}}">
		<link rel="stylesheet" type="text/css" media="all" href="{{asset('style-custom.less')}}"/>
		<link rel="stylesheet" type="text/css" media="all" href="{{asset('style-custom-responsive.less')}}"/>


		<!-- Fonts -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto%7CRoboto+Slab%7CRoboto+Condensed&amp;subset=latin">


		<!-- JQuery Libs -->
		<script type="text/javascript" src="{{resources('js/template/jquery.js')}}"></script>
		<script type="text/javascript" src="{{resources('js/template/jquery-migrate.min.js')}}"></script>
	</head>



	<body class="home page page-id-20 page-template page-template-template-visual-composer page-template-template-visual-composer-php has-top-grid  sidebar-border vc-block-fx vc-block-border wpb-js-composer js-comp-ver-5.1 vc_responsive" itemscope="" itemtype="http://schema.org/WebPage" style="transform: none;">
		<!--Google Anlytics-->
		@include('components.google-analytics')

		<!-- Aside right -->
		@include('template.aside-right')

		<!-- Menu left -->
		@include('template.menu-left')

		<!-- Content -->
		<div id="page-outer-wrap" style="transform: none;">
			<div id="page-inner-wrap" class="mm-page mm-slideout" style="transform: none;">


				<!-- Menu mobile -->
				@include('template.menu-mobile')

				<!-- Menu  -->
				@include('template.menu')

				<!-- Header content-->
				@yield('header-content')

				<div id="content-container" style="transform: none;">
					<div class="container content-shadow" style="transform: none;">
						<div id="page-content" class="loop-single right-sidebar loop-page-2 clearfix" style="transform: none;">
							<div id="main" class="main no-top-bottom">
								<!-- Body content-->
								@yield('content')
							</div>

							<!-- Sidebar -->
							@yield('sidebar')

							<!-- Overlay -->
							@include('template.overlay')

						</div>
					</div>
				</div>

				<!-- Footer-->
				@include('template.footer')

			</div>
		</div>


		<!-- Btn to top -->
		@include('components.btn-to-top')

		<!-- Cookies Bar -->
		@include('components.cookies-bar')







		<!--SCRIPTS-->
		<script type="text/javascript" src="{{resources('js/template/js.cookie.js')}}"></script>
		<script type="text/javascript" src="{{resources('js/cookies-bar.js')}}"></script>
		<script type="text/javascript" src="{{resources('js/aside-right.js')}}"></script>


		<script>
/*<![CDATA[*/"use strict";
var miptheme_smooth_scrolling = true;
var miptheme_ajaxpagination_timer = 1500;
var miptheme_sticky_sidebar_margin = 75;
var mipthemeLocalCache = {};
(function () {
	"use strict";
	mipthemeLocalCache = {
		data: {},
		remove: function (resource_id) {
			delete mipthemeLocalCache.data[resource_id];
		},
		exist: function (resource_id) {
			return mipthemeLocalCache.data.hasOwnProperty(resource_id) && mipthemeLocalCache.data[resource_id] !== null;
		},
		get: function (resource_id) {
			return mipthemeLocalCache.data[resource_id];
		},
		set: function (resource_id, cachedData) {
			mipthemeLocalCache.remove(resource_id);
			mipthemeLocalCache.data[resource_id] = cachedData;
		}
	};
})();
/*]]>*/
		</script>
		<script type="text/javascript" src="{{resources('js/template/editor.js')}}"></script>
		<script type="text/javascript" src="{{resources('js/template/bootstrap.min.js')}}"></script>
		<script type="text/javascript" src="{{resources('js/template/mip.external.min.js')}}"></script>
		<script type="text/javascript">
var miptheme_ajax_url = {"ajaxurl": "http:\/\/newsland.newsthemes.info\/wp-admin\/admin-ajax.php"};
		</script>
		<script type="text/javascript" src="{{resources('js/template/functions.js')}}"></script>
		<script type="text/javascript" src="{{resources('js/template/wp-embed.min.js')}}"></script>
		<script type="text/javascript" src="{{resources('js/template/js_composer_front.min.js')}}"></script>
		<script type="text/javascript" src="{{resources('js/template/comment-reply.min.js')}}"></script>
		<script type="text/javascript" src="{{resources('js/template/isotope.pkgd.min.js')}}"></script>
		<!--Gmaps-->
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAc_gnjmRrsOPi6K1zohqg4RLqpEP_YqOs&callback=initMap"></script>
		<!--Custom Scripts-->
		<script type="text/javascript" src="{{resources('js/functions-custom.js')}}"></script>
	</body>
</html>