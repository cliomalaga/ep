@inject('functions', 'App\Repositories\Functions\FunctionsRepository')
@inject('controller', 'App\Http\Controllers\Controller')

@php
$menu = $functions->getMenu();
@endphp


<header id="page-header" class="hidden-xs hidden-sm wrap-header-layout-2 clearfix">
	<div class="container">
		<div id="header-branding" class="header-layout-2">
			<div class="row">
				<div class="col-md-6" >

					<h1>
						<a itemprop="url" href="{{ url('/') }}">
							<img class="logo-principal" src="{{resources('images/ep/LogoFullWhite-NotBook.png')}}" alt="Evaluación Psicológica">
						</a>
					</h1>
				</div>
				<div class="col-md-3 col-md-offset-3 text-center">
					<div class="wrap-container">

						<form id="search-form"  method="POST" action="buscador">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="text" id="input-search-menu" name="search" placeholder="Buscar ..." value="">
							<button id="button-search-menu">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="sticky-header-wrapper" style="height: 50px;">
		<div id="sticky-header" data-spy="affix" data-offset-top="150" class="affix-top">
			<div id="header-navigation">
				<nav id="main-menu" class="container relative">
					<!--
					<a href="{{ url('/') }}">
						<span class="sticky-logo" title="Evaluación Psicológica" alt="Evaluación Psicológica">
							<img class="logo-sticky" src="{{resources('images/ep/Logo.png')}}" alt="Evaluación Psicológica">
						</span>
					</a>
					-->
					<ul id="menu-main-nav-1" class="nav clearfix">
						<li id="nav-menu-item-409" class="main-menu-item  menu-item-even menu-item-depth-0 has-icon menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
							<a href="#" class="menu-link main-menu-link">Nosotros</a>
							<div class="dropnav-container" style="display: none;">
								<ul class="dropnav-menu">
									<li id="" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-4 ">
										<a href="{{ url('quienes-somos') }}" class="menu-link sub-menu-link">Quienes somos</a>
									</li>
									<li id="" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-4 ">
										<a href="{{ url('admision-socios') }}" class="menu-link sub-menu-link">Admision Socios</a>
									</li>
									<li id="" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-4 ">
										<a href="{{ url('cuota-socios') }}" class="menu-link sub-menu-link">Cuotas Socios</a>
									</li>
									<li id="" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-4 ">
										<a href="{{ url('contacto') }}" class="menu-link sub-menu-link">Contacto</a>
									</li>
									<!--
									<li id="" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-4 ">
										<a href="{{ url('miembros') }}" class="menu-link sub-menu-link">Miembros</a>
									</li>
									-->
									<!--										
									<li id="" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-4 ">
											<a href="{{ url('objetivos') }}" class="menu-link sub-menu-link">Objetivos</a>
										</li>
									-->
									<li id="" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-4 ">
										<a href="{{ url('creative-commons') }}" class="menu-link sub-menu-link">Creative Commmons</a>
									</li>
								</ul>
							</div>
						</li>


						@if(!empty($menu))


						@php
						$section = 'Pruebas'
						@endphp
						<li id="nav-menu-item-409" class="main-menu-item  menu-item-even menu-item-depth-0 has-icon menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
							<a href="{{ url('/'.str_slug($section)) }}" class="menu-link main-menu-link">{{ $section }}</a>
							<div class="dropnav-container" style="display: none;">
								<ul class="dropnav-menu">

									@foreach($menu->submenu1 as $item)
									<li id="nav-menu-item-412" class=" menu-item-odd menu-item-depth-1 menu-item menu-item-type-custom menu-item-object-custom">
										<a href="{{ url('/'.str_slug($section).'/'.str_slug( $item->name)) }}" class="menu-link ">{{ $item->name }}</a>
									</li>
									@endforeach
								</ul>
							</div>
						</li>




						@php
						$section = 'Preguntas'
						@endphp
						<li id="nav-menu-item-409" class="main-menu-item  menu-item-even menu-item-depth-0 has-icon menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
							<a href="{{ url('/'.str_slug($section)) }}" class="menu-link main-menu-link">{{ $section }}</a>
							<div class="dropnav-container" style="display: none;">
								<ul class="dropnav-menu">

									@foreach($menu->submenu2 as $item)
									<li id="nav-menu-item-412" class=" menu-item-odd menu-item-depth-1 menu-item menu-item-type-custom menu-item-object-custom">
										<a href="{{ url('/'.str_slug($section).'/'.str_slug( $item->name)) }}" class="menu-link ">{{ $item->name }}</a>
									</li>
									@endforeach
								</ul>
							</div>
						</li>


						@if ($controller->isLogged())
						@php
						$section = 'Documentos'
						@endphp
						<li id="nav-menu-item-409" class="main-menu-item  menu-item-even menu-item-depth-0 has-icon menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
							<a href="{{ url('/'.str_slug($section)) }}" class="menu-link main-menu-link">{{ $section }}</a>
							<div class="dropnav-container" style="display: none;">
								<ul class="dropnav-menu">

									@foreach($menu->submenu3 as $item)
									<li id="nav-menu-item-412" class=" menu-item-odd menu-item-depth-1 menu-item menu-item-type-custom menu-item-object-custom">
										<a href="{{ url('/'.str_slug($section).'/'.str_slug( $item->name)) }}" class="menu-link ">{{ $item->name }}</a>
									</li>
									@endforeach
								</ul>
							</div>
						</li>
						@endif


						@php
						$section = 'Enseñanzas'
						@endphp
						<li id="nav-menu-item-409" class="main-menu-item  menu-item-even menu-item-depth-0 has-icon menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
							<a href="{{ url('/'.str_slug($section)) }}" class="menu-link main-menu-link">{{ $section }}</a>
							<div class="dropnav-container" style="display: none;">
								<ul class="dropnav-menu">

									@foreach($menu->submenu4 as $item)
									<li id="nav-menu-item-412" class=" menu-item-odd menu-item-depth-1 menu-item menu-item-type-custom menu-item-object-custom">
										<a href="{{ url('/'.str_slug($section).'/'.str_slug( $item->name)) }}" class="menu-link ">{{ $item->name }}</a>
									</li>
									@endforeach
								</ul>
							</div>
						</li>



						@php
						$section = 'Casos'
						@endphp
						<li id="nav-menu-item-409" class="main-menu-item  menu-item-even menu-item-depth-0 has-icon menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
							<a href="{{ url('/'.str_slug($section)) }}" class="menu-link main-menu-link">{{ $section }}</a>
							<div class="dropnav-container" style="display: none;">
								<ul class="dropnav-menu">

									@foreach($menu->submenu5 as $item)
									<li id="nav-menu-item-412" class=" menu-item-odd menu-item-depth-1 menu-item menu-item-type-custom menu-item-object-custom">
										<a href="{{ url('/'.str_slug($section).'/'.str_slug( $item->name)) }}" class="menu-link ">{{ $item->name }}</a>
									</li>
									@endforeach
								</ul>
							</div>
						</li>



						@php
						$section = 'Investigaciones'
						@endphp
						<li id="nav-menu-item-409" class="main-menu-item  menu-item-even menu-item-depth-0 has-icon menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
							<a href="{{ url('/'.str_slug($section)) }}" class="menu-link main-menu-link">{{ $section }}</a>
							<div class="dropnav-container" style="display: none;">
								<ul class="dropnav-menu">

									@foreach($menu->submenu6 as $item)
									<li id="nav-menu-item-412" class=" menu-item-odd menu-item-depth-1 menu-item menu-item-type-custom menu-item-object-custom">
										<a href="{{ url('/'.str_slug($section).'/'.str_slug( $item->name)) }}" class="menu-link ">{{ $item->name }}</a>
									</li>
									@endforeach
								</ul>
							</div>
						</li>
						@endif






						<li class="search-nav">
							<a id="search-nav-button" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-search"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<form id="search-form"  method="POST" action="buscador">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="text" id="input-search-menu" name="search" placeholder="Buscar ..." value="">
									<button id="button-search-menu">
										<span class="glyphicon glyphicon-search"></span>
									</button>
								</form>
							</div>
						</li>





						<li class="menu-login">
							@if ($controller->isLogged())
							<a href="{{ url('cerrar-sesion') }}" alt="Cerrar Sesión" title="Cerrar Sesión">
								<i class="fa fa-sign-out"></i>
							</a>
							@else
							<a href="{{ url('login') }}" alt="Login" title="Login">
								<i class="fa fa-lock"></i>
							</a>
							@endif
						</li>





						<li class="logo-seaep">
							<img class="" src="{{resources('images/ep/seaep.jpg')}}" alt="Sociedad Española para el Avance de la Evaluación Psicológica" title="Sociedad Española para el Avance de la Evaluación Psicológica">
						</li>






					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>