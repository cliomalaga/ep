<nav id="mobile-menu" class="mm-menu mm-horizontal mm-offcanvas mm-front">
	<a class="logo-menu-left" itemprop="url" href="{{ url('/') }}">
		<img src="{{resources('images/ep/LogoFullWhite.png')}}" alt="Evaluación Psicológica">
	</a>

	<form id="search-form-mobile" class="mm-search" method="get" action="/">
		<input type="text" name="search" placeholder="Buscar..." value="">
		<button id="button-search-menu">
			<span class="glyphicon glyphicon-search"></span>
		</button>
	</form>
	<ul id="menu-main-nav" class="nav clearfix mm-list mm-panel mm-opened mm-current">
		<li id="mobile-nav-menu-item-553" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="{{ url('/') }}">Home</a>
		</li>
		<li id="mobile-nav-menu-item-408" class="main-menu-item  menu-item-even menu-item-depth-0 has-icon menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
			<a class="mm-subopen" href="#mm-0"></a>
			<a href="#mm-0">Nosotros</a>
		</li>
		<li id="mobile-nav-menu-item-409" class="main-menu-item  menu-item-even menu-item-depth-0 has-icon menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-has-children ">
			<a href="">Pruebas</a>
		</li>
		<li id="mobile-nav-menu-item-411" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-4 ">
			<a href="">Preguntas</a>
		</li>
		<li id="mobile-nav-menu-item-422" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-7 ">
			<a href="">Documentos</a>
		</li>
		<li id="mobile-nav-menu-item-410" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-13 ">
			<a href="">Enseñanzas</a>
		</li>
		<li id="mobile-nav-menu-item-411" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-13 ">
			<a href="">Casos</a>
		</li>
		<li id="mobile-nav-menu-item-412" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-taxonomy menu-item-object-category menu-category-13 ">
			<a href="">Investigaciones</a>
		</li>
	</ul>


	<!--NOSOTROS-->
	<ul class="sub-menu mm-list mm-panel mm-hidden" id="mm-0">
		<li class="mm-subtitle">
			<a class="mm-subclose" href="#menu-main-nav">Nosotros</a>
		</li>
		<li id="mobile-nav-menu-item-412" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children ">
			<a class="mm-subopen" href="#mm-01"></a>
			<a href="#mm-01">¿Quiénes somos?</a>
		</li>
		<li id="mobile-nav-menu-item-413" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
			<a class="mm-subopen" href="#mm-02"></a>
			<a href="#mm-02">¿Qué es la evaluación psicológica?</a>
		</li>
		<li id="mobile-nav-menu-item-417" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
			<a class="mm-subopen" href="#mm-03"></a>
			<a href="#mm-03">Síguenos en RRSS</a>
		</li>
	</ul>
	<!--¿Quiénes somos?-->
	<ul class="sub-menu mm-list mm-panel mm-hidden" id="mm-01">
		<li class="mm-subtitle">
			<a class="mm-subclose" href="#mm-0">¿Quiénes somos?</a>
		</li>
		<li id="mobile-nav-menu-item-543" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="">Noticias</a>
		</li>
		<li id="mobile-nav-menu-item-542" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">Miembros</a>
		</li>
		<li id="mobile-nav-menu-item-541" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">Objetivos</a>
		</li>
		<li id="mobile-nav-menu-item-540" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">Creative Commmons</a>
		</li>
		<li id="mobile-nav-menu-item-540" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">Evaluación Psicológica</a>
		</li>
		<li id="mobile-nav-menu-item-540" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">Diagnóstico Psicológica</a>
		</li>
		<li id="mobile-nav-menu-item-540" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">Tipo de Evaluaciones</a>
		</li>
	</ul>
	<!--¿Qué es la evaluación psicológica?-->
	<ul class="sub-menu mm-list mm-panel mm-hidden" id="mm-02">
		<li class="mm-subtitle">
			<a class="mm-subclose" href="#mm-0">¿Qué es la evaluación psicológica?</a>
		</li>
		<li id="mobile-nav-menu-item-543" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="">Utilidad</a>
		</li>
		<li id="mobile-nav-menu-item-542" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">Proceso</a>
		</li>
		<li id="mobile-nav-menu-item-541" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">Componentes</a>
		</li>
		<li id="mobile-nav-menu-item-540" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">Evaluación Clínica</a>
		</li>
		<li id="mobile-nav-menu-item-540" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">Evaluación de RRH</a>
		</li>
	</ul>
	<!--Síguenos-->
	<ul class="sub-menu mm-list mm-panel mm-hidden" id="mm-03">
		<li class="mm-subtitle">
			<a class="mm-subclose" href="#mm-0">Síguenos</a>
		</li>
		<li id="mobile-nav-menu-item-543" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-20 current_page_item ">
			<a href="">
				<i class="fa fa-fw fa-facebook"></i> Facebook
			</a>
		</li>
		<li id="mobile-nav-menu-item-542" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">
				<i class="fa fa-fw fa-twitter"></i> Twitter
			</a>
		</li>
		<li id="mobile-nav-menu-item-541" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">
				<i class="fa fa-fw fa-twitter"></i> Twitter
			</a>
		</li>
		<li id="mobile-nav-menu-item-540" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">
				<i class="fa fa-fw fa-google-plus"></i> Google+
			</a>
		</li>
		<li id="mobile-nav-menu-item-540" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">
				<i class="fa fa-fw fa-linkedin"></i> Linkedin
			</a>
		</li>
		<li id="mobile-nav-menu-item-540" class="sub-menu-item sub-sub-menu-item menu-item-even menu-item-depth-2 menu-item menu-item-type-post_type menu-item-object-page ">
			<a href="">
				<i class="fa fa-fw fa-youtube"></i> Youtube
			</a>
		</li>
	</ul>
</nav>