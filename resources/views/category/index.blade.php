@extends('template.master')

@section('title')
|
@if(!empty($subcategory_name))
{{ ucwords(mb_strtolower($subcategory_name)) }}
@else
{{ ucwords(mb_strtolower($category_name)) }}
@endif
@endsection

@section('sidebar')
@include('category.sidebar-category')
@endsection



@section('content')
<div class="content-container">
	<header>
		<div class="breadcrumb">
			<span>

			</span>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a href="{{ url('/') }}" rel="v:url" property="v:title" class="home">
					Home
				</a>
			</div>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				@if(!empty($subcategory_name))
				<a href="{{ url($category_url) }}" title="{{ ucwords(mb_strtolower($category_name)) }}" rel="v:url" property="v:title">
					{{ ucwords(mb_strtolower($category_name)) }}
				</a>
				@else
				<a title="{{ $category_name }}" rel="v:url" property="v:title">
					{{ ucwords(mb_strtolower($category_name)) }}
				</a>
				@endif
			</div>
			@if(!empty($subcategory_name))
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a title="{{ $subcategory_name }}" rel="v:url" property="v:title">
					{{ ucwords(mb_strtolower($subcategory_name)) }}
				</a>
			</div>
			@endif
		</div>

		<h2 class="h2-categories">
			<span>
				@if(!empty($subcategory_name))
				{{ ucwords(mb_strtolower($subcategory_name)) }}
				@else
				{{ ucwords(mb_strtolower($category_name)) }}
				@endif
			</span>
		</h2>
	</header>


	<div class="row filter-category">
		<div class="col-sm-6">
			<p id="search-form" class="search-category">
				<input id="input-search-page" type="text" class="quicksearch" placeholder="Buscar..." />
				<button id="button-search-menu">
					<span class="glyphicon glyphicon-search"></span>
				</button>
			</p>

		</div>
		<div class="col-sm-6">
			<div class="btn-group sort-button-group ">
				<button class="btn btn-default" data-sort-direction="asc" data-sort-value="filedate" type="button">Fecha <span aria-hidden="true" class="glyphicon glyphicon-chevron-up"></span></button>
				<button class="btn btn-default" data-sort-direction="asc" data-sort-value="fileby" type="button">Titulo <span aria-hidden="true" class="glyphicon glyphicon-chevron-up"></span></button>
			</div>
		</div>
	</div>

	<br>




	<div class="cat-layout clearfix page-categories grid">
		@if(!empty($items))
		@foreach($items as $item)
		<div class="row">
			<article class="def article-post type-image element-item">
				<figure class="overlay relative">
					<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
						<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
					</a>
					<figcaption>
						<div class="entry-meta"></div>
					</figcaption>
				</figure>
				<div class="entry">
					<header>
						<h2 class="h2-categories item">
							<a href="{{ url($item->url) }}">
								<span class="file-by">
									{{ ucfirst(str_limit($item->title, $limit = 150, $end = '...')) }}
								</span>
							</a>
						</h2>
					</header>
					<div class="entry-meta">
						<time class="file-date hidden">
							{{ $item->created_at }}
						</time>
						<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">
							{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}
						</time>
						@if(!empty($item->created_by))
						<span itemprop="author" class="entry-author hidden-xs">
							<a href="{{ url($item->created_by->url) }}">
								{{ ucwords(mb_strtolower($item->created_by->name)) }} {{ ucwords(mb_strtolower($item->created_by->lastname)) }}
							</a>
						</span>
						@endif
						<span itemprop="categories" class="entry-categories">
							<a href="{{ url($item->category->url) }}" title="{{ ucwords(mb_strtolower($item->category->name)) }}">
								{{ ucwords(mb_strtolower($item->category->name)) }}
							</a>
							|
							<a href="{{ url($item->subcategory->url) }}" title="{{ ucwords(mb_strtolower($item->subcategory->name)) }}">
								{{ ucwords(mb_strtolower($item->subcategory->name)) }}
							</a>
						</span>
					</div>
					<span class="text">
						<p>
							@if(!empty($item->text_pretty))
							{{ ucfirst(str_limit($item->text_pretty, $limit = 200, $end = '...')) }}
							@endif
						</p>
						<a class="more-link" itemprop="url" href="{{ url($item->url) }}">
							Leer más
						</a>
					</span>
				</div>
			</article>
		</div>
		@endforeach






		@else

		@include('components.empty-items')

		@endif
	</div>

	@if(!$isArchive && !empty($items))
	<div class="row">
		<div class="col-sm-12">
			<div class="read-more-category">
				@php
				$name = (!empty($subcategory_name))? $subcategory_name : $category_name;
				$string = 'Ver todos los artículos sobre ' . $name
				@endphp
				@if(!empty($subcategory_url))
				<a href="{{ url('biblioteca/' . $category_url . '/' . $subcategory_url) }}">
					{{ $string }}
				</a>
				@else
				<a href="{{ url('biblioteca/' . $category_url) }}">
					{{ $string }}
				</a>
				@endif
			</div>
		</div>
	</div>
	@endif


	<br>
	<br>
	<br>
	<br>
</div>

@endsection