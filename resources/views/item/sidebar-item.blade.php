<div id="sidebar" class="sidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
	<div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static;">
		<!--		<div class="widget ad ad-300x250">
					<a href="http://sportsland.newsthemes.info/" target="_blank">
						<img src="{{resources('images/template/banner_300_sportsland.jpg')}}" width="300" alt="">
					</a>
				</div>-->




		@include('sidebar.sidebar-category')

		<br>

		@include('sidebar.sidebar-subcategory')

		<br>

		@include('sidebar.sidebar-last')

		<br>

		@include('sidebar.sidebar-timeline')

		<br>

		@include('sidebar.sidebar-archive')

		<br>

		@include('sidebar.sidebar-gallery')

	</div>
</div>