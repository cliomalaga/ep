@extends('template.master')

@section('title')
| {{ $item->title }}
@endsection

@section('sidebar')
@include('item.sidebar-item')
@endsection



@section('content')
<div class="content-container">

	<article id="post-297" class="article-post clearfix post-297 post type-post status-publish format-image has-post-thumbnail hentry category-pc category-ps4 category-wii-u category-xbox-one post_format-post-format-image" itemscope="" itemtype="http://schema.org/Article">
		<!--Item-->
		<header>
			<div class="breadcrumb">
				<span>

				</span>
				<div class="vbreadcrumb" typeof="v:Breadcrumb">
					<a href="{{ url('/') }}" rel="v:url" property="v:title" class="home">
						Home
					</a>
				</div>
				<div class="vbreadcrumb" typeof="v:Breadcrumb">
					<a href="{{ url($item->category->url) }}" title="{{ $item->category->name }}" rel="v:url" property="v:title">
						{{ ucwords(mb_strtolower($item->category->name)) }}
					</a>
				</div>
				<div class="vbreadcrumb" typeof="v:Breadcrumb">
					<a href="{{ url($item->subcategory->url) }}" title="{{ $item->subcategory->name }}" rel="v:url" property="v:title">
						{{ ucwords(mb_strtolower($item->subcategory->name)) }}
					</a>
				</div>
			</div>
			<h2 class="h2-categories">
				<span>
					{{ $item->title }}
				</span>
			</h2>
			<div class="entry-meta">
<!--				<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">
					{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}
				</time>-->
				@if(!empty($item->created_by))
				<span class="entry-author" itemprop="author">
					<a href="{{ url($item->created_by->url) }}">
						{{ ucwords(mb_strtolower($item->created_by->name)) }} {{ ucwords(mb_strtolower($item->created_by->lastname)) }}
					</a>
				</span>
				@endif
				<span class="entry-categories">
					<a href="{{ url($item->category->url) }}">
						{{ ucwords(mb_strtolower($item->category->name)) }}
					</a>
					|
					<a href="{{ url($item->subcategory->url) }}">
						{{ ucwords(mb_strtolower($item->subcategory->name)) }}
					</a>
				</span>
			</div>
		</header>
		<div class="image-item head-image thumb-wrap relative">
			<img src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" itemprop="image">
		</div>




		<div class="article-post-content clearfix">
			<br>
			<div class="lead">
				<div class="padding-style-1">
					<h2>
						{{ ucfirst($item->title) }}
					</h2>
					<p>
						{{ ucfirst($item->subtitle) }}
					</p>
				</div>
			</div>
			<div class="padding-style-1">
				<p>
					@if(!empty($item->text))
					{!! ucfirst($item->text) !!}
					@endif
				</p>
				@if(!empty($item->attached))
				<a href="{{ urlDashboard($item->attached) }}" download target="_self" title="Descargar adjunto" alt="Descargar adjunto">
					<i class="fa fa-file"></i>
					{{ onlyAttachedName($item->attached) }}
				</a>
				@endif
			</div>

			<br>
			<!--		<div class="ad ad-cnt ad-300x250 ad-cnt-left ad-cnt-hide-on-mobile-no">
						<a href="http://sportsland.newsthemes.info/" target="_blank">
							<img src="http://newsland.newsthemes.info/wp-content/uploads/2017/04/banner_300_sportsland.jpg?361cfd" width="300" alt="">
						</a>
					</div>-->
		</div>


		@include('item.share')



		<!--Previous and next item-->
		<aside class="post-navigation clearfix">
			<div class="row">
				<div class="col-md-6">
					@if(!empty($previous))
					<a href="{{ url($previous->url) }}" title="{{ ucfirst($previous->title) }}">
						<cite>< Artículo anterior </cite>
					</a>
					{{ ucfirst(str_limit($previous->title, $limit = 100, $end = '...')) }}
					@endif
				</div>
				<div class="col-md-6 text-right">
					@if(!empty($next))
					<a href="{{ url($next->url) }}" title="{{ ucfirst($next->title) }}">
						<cite>Artículo siguiente ></cite>
					</a>
					{{ ucfirst(str_limit($next->title, $limit = 100, $end = '...')) }}
					@endif
				</div>
			</div>
		</aside>







		<!--Related Articles-->
		<aside id="related-posts" class="posts-related loop-cat standard loop-cat-3">
			<header>
				<h2>
					<span>Artículos relacionados</span>
				</h2>
			</header>
			<div class="cat-layout related-items">
				<div class="row">
					@foreach($relateds as $item)
					<div class="col-sm-4">
						<article class="def def-medium">
							<figure class="overlay relative">
								<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
									<img itemprop="image" src="{{urlDashboard($item->image)}}" width="277" height="190" alt="{{ ucfirst($item->title) }}" class="img-responsive">
								</a>
								<figcaption>
									<div class="entry-meta"></div>
								</figcaption>
							</figure>
							<div class="entry">
								<span class="entry-category parent-cat-1 cat-1">
									<a itemprop="url" href="{{ url($item->subcategory->url) }}">
										{{ ucwords(mb_strtolower($item->subcategory->name)) }}
									</a>
								</span>
								<h3 itemprop="name">
									<a itemprop="url" href="{{ url($item->url) }}">
										{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
									</a>
								</h3>
								<div class="entry-meta">
									<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">
										{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}
									</time>
								</div>
							</div>
						</article>
					</div>
					@endforeach
				</div>
			</div>
		</aside>



		<br>

		<!-- Other users viewed -->
		<aside id="others-viewed" class="posts-related loop-cat standard loop-cat-3">
			@include('components.others-viewed')
		</aside>


		<br>
		<br>
		<br>


	</article>
</div>
@endsection