<!--Share RRSS-->
<meta itemprop="datePublished" content="{{ $item->created_at }}">
<meta itemprop="dateModified" content="{{ $item->created_at }}">
<meta itemprop="headline" content="{{ $item->title }}">
<div itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">
	<div itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
		<meta itemprop="url" content="{{resources('images/ep/LogoFullWhite.png')}}">
		<meta itemprop="width" content="">
		<meta itemprop="height" content="">
	</div>
	<meta itemprop="name" content="Evaluación Psicológica">
</div>
<div class="soc-media-sharing soc-style-three">
	<a class="btn-social btn-facebook btn-icon-title" href="http://www.facebook.com/sharer.php?u={{url($item->url)}}"
	   onclick="window.open(this.href, 'weeklywin', 'left=50,top=50,width=600,height=360,toolbar=0'); return false;">
		<i class="fa fa-facebook-square fa-lg"></i>
		<span id="smFacebook">Facebook</span>
	</a>
	<a class="btn-social btn-twitter btn-icon-title" href="https://twitter.com/intent/tweet?text={{$item->title}};url={{url($item->url)}}"
	   onclick="window.open(this.href, 'weeklywin', 'left=50,top=50,width=600,height=360,toolbar=0'); return false;">
		<i class="fa fa-twitter-square fa-lg"></i>
		<span id="smTwitter">Twitter</span>
	</a>
	<a class="btn-social btn-google btn-icon-title" href="http://plus.google.com/share?url={{url($item->url)}}"
	   onclick="window.open(this.href, 'weeklywin', 'left=50,top=50,width=600,height=360,toolbar=0'); return false;">
		<i class="fa fa-google-plus-square fa-lg"></i>
		<span id="smGoogle">Google+</span>
	</a>
	<a class="btn-social btn-linkedin btn-icon-title" href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{url($item->url)}};title={{$item->title}}"
	   onclick="window.open(this.href, 'weeklywin', 'left=50,top=50,width=600,height=360,toolbar=0'); return false;">
		<i class="fa fa-linkedin-square fa-lg"></i>
		<span id="smLinkedin">LinkedIn</span		>
	</a		>
	<a class="btn-social btn-pinterest btn-icon-title" href="http://pinterest.com/pin/create/button/?url={{url($item->url)}}&amp;media={{urlDashboard($item->image)}}"
	   onclick="window.open(this.href, 'weeklywin', 'left=50,top=50,width=600,height=360,toolbar=0'); return false;"		   >
		<i class="fa fa-pinterest-square fa-lg"></i>
		<span>Pinterest</span>
	</a>
</div>