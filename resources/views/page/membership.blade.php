@extends('template.master')

@section('title')
| Cuota de Socios
@endsection

@section('sidebar')
@include('page.sidebar-page')
@endsection



@section('content')
<div class="content-container">
	<header>
		<div class="breadcrumb">
			<span>

			</span>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a href="{{ url('/') }}" rel="v:url" property="v:title" class="home">
					Home
				</a>
			</div>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a title="Cuota de Socios" rel="v:url" property="v:title">
					Cuota de Socios
				</a>
			</div>
		</div>

		<h2 class="h2-categories">
			<span>
				Cuota de Socios
			</span>
		</h2>
	</header>



	<div class="cat-layout clearfix page-static">
		<div class="row">
			<div class="col-md-12">

				<br>

				<h2>
					Afiliación a SEAEP
				</h2>

				<p>
					Para solicitar la afiliación a la Sociedad Española para el Avance de la Evaluación Psicológica, es necesario bajarse el documento adjunto, rellenar los datos personales e informaciones necesarias, incluyendo la forma de pago de las cuotas, y enviarlo al correo electrónico de la sociedad SEAEP2@gmail.com
				</p>

				<br>
				<a href="http://evaluacionpsicologica.es/dashboard/public/attacheds/documents/20171221_SEAEP_Formulario_afiliacion.doc" download="" target="_self" title="Descargar adjunto" alt="Descargar adjunto">
					<i class="fa fa-file"></i>
					SEAEP_Formulario_afiliacion.doc
				</a>
				<br>
				<br>
				<br>
				<br>
			</div>
		</div>
	</div>
</div>

@endsection