@extends('template.master')

@section('title')
| Política de Cookies
@endsection

@section('sidebar')
@include('page.sidebar-page')
@endsection



@section('content')
<div class="content-container">
	<header>
		<div class="breadcrumb">
			<span>

			</span>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a href="{{ url('/') }}" rel="v:url" property="v:title" class="home">
					Home
				</a>
			</div>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a title="Política de Cookies" rel="v:url" property="v:title">
					Política de Cookies
				</a>
			</div>
		</div>

		<h2 class="h2-categories">
			<span>
				Política de Cookies
			</span>
		</h2>
	</header>



	<div class="cat-layout clearfix page-static">
		<div class="row">
			<div class="col-md-12">
				<br>

				<p>
					Cookie es un fichero que se descarga en su ordenador al acceder a determinadas páginas web. Las cookies permiten a una página web, entre otras cosas, almacenar y recuperar información sobre los hábitos de navegación de un usuario o de su equipo y, dependiendo de la información que contengan y de la forma en que utilice su equipo, pueden utilizarse para reconocer al usuario.. El navegador del usuario memoriza cookies en el disco duro solamente durante la sesión actual ocupando un espacio de memoria mínimo y no perjudicando al ordenador. Las cookies no contienen ninguna clase de información personal específica, y la mayoría de las mismas se borran del disco duro al finalizar la sesión de navegador (las denominadas cookies de sesión).
					<br>
					La mayoría de los navegadores aceptan como estándar a las cookies y, con independencia de las mismas, permiten o impiden en los ajustes de seguridad las cookies temporales o memorizadas.
				</p>

				<br>

				<h2>
					¿Qué tipos de cookies utiliza esta página web?
				</h2>

				<p>
					– <b>Cookies técnicas:</b> Son aquéllas que permiten al usuario la navegación a través de una página web, plataforma o aplicación y la utilización de las diferentes opciones o servicios que en ella existan como, por ejemplo, controlar el tráfico y la comunicación de datos, identificar la sesión, acceder a partes de acceso restringido, recordar los elementos que integran un pedido, realizar el proceso de compra de un pedido, realizar la solicitud de inscripción o participación en un evento, utilizar elementos de seguridad durante la navegación, almacenar contenidos para la difusión de videos o sonido o compartir contenidos a través de redes sociales.
				</p>

				<p>
					– <b>Cookies de personalización:</b> Son aquéllas que permiten al usuario acceder al servicio con algunas características de carácter general predefinidas en función de una serie de criterios en el terminal del usuario como por ejemplo serian el idioma, el tipo de navegador a través del cual accede al servicio, la configuración regional desde donde accede al servicio, etc.
				</p>

				<p>
					– <b>Cookies de análisis:</b> Son aquéllas que bien tratadas por nosotros o por terceros, nos permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios del servicio ofertado. Para ello se analiza su navegación en nuestra página web con el fin de mejorar la oferta de productos o servicios que le ofrecemos.
				</p>

				<p>
					– <b>Cookies publicitarias:</b> Son aquéllas que, bien tratadas por nosotros o por terceros, nos permiten gestionar de la forma más eficaz posible la oferta de los espacios publicitarios que hay en la página web, adecuando el contenido del anuncio al contenido del servicio solicitado o al uso que realice de nuestra página web. Para ello podemos analizar sus hábitos de navegación en Internet y podemos mostrarle publicidad relacionada con su perfil de navegación.
				</p>

				<p>
					– <b>Cookies de publicidad comportamental:</b> Son aquellas que permiten la gestión, de la forma más eficaz posible, de los espacios publicitarios que, en su caso, el editor haya incluido en una página web, aplicación o plataforma desde la que presta el servicio solicitado. Estas cookies almacenan información del comportamiento de los usuarios obtenida a través de la observación continuada de sus hábitos de navegación, lo que permite desarrollar un perfil específico para mostrar publicidad en función del mismo.
				</p>

				<p>
					– <b>Cookies de terceros:</b> En particular, este sitio Web utiliza Google Analytics, un servicio analítico de web prestado por Google, Inc. con domicilio en los Estados Unidos con sede central en 1600 Amphitheatre Parkway, Mountain View, California 94043.  Para la prestación de estos servicios, estos utilizan cookies que recopilan la información, incluida la dirección IP del usuario, que será transmitida, tratada y almacenada por Google en los términos fijados en la Web Google.com. Incluyendo la posible transmisión de dicha información a terceros por razones de exigencia legal o cuando dichos terceros procesen la información por cuenta de Google.
				</p>

				<p>
					El Usuario acepta expresamente, por la utilización de este Site, el tratamiento de la información recabada en la forma y con los fines anteriormente mencionados. Y asimismo reconoce conocer la posibilidad de rechazar el tratamiento de tales datos o información rechazando el uso de Cookies mediante la selección de la configuración apropiada a tal fin en su navegador. Si bien esta opción de bloqueo de Cookies en su navegador puede no permitirle el uso pleno de todas las funcionalidades del Website.
					Puede usted permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones del navegador instalado en su ordenador:
				</p>

				<ul>
					<li>
						<a target="_blank" href="https://support.google.com/chrome/answer/95647?hl=es">
							Chrome
						</a>
					</li>
					<li>
						<a target="_blank" href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectlocale=es&redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we">
							Firefox
						</a>
					</li>
					<li>
						<a target="_blank" href="https://support.apple.com/kb/ph5042?locale=es_ES">
							Safari
						</a>
					</li>
					<li>
						<a target="_blank" href="https://support.microsoft.com/es-es/products/windows?os=windows-7">
							Explorer
						</a>
					</li>
				</ul>

				<br>

				<p>
					Si tiene dudas sobre esta política de cookies, puede contactar en <a href="mailto:info@evaluacionpsicologica.es">info@evaluacionpsicologica.es</a> o a través de nuestro formulario de <a href="{{ url('contacto') }}">contacto</a>.
				</p>


				<br>
				<br>
				<br>
				<br>

			</div>
		</div>
	</div>
</div>

@endsection