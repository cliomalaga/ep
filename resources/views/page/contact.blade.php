@extends('template.master')

@section('title')
| Contacto
@endsection

@section('sidebar')
@include('page.sidebar-page')
@endsection



@section('content')
<div class="content-container">
	<header>
		<div class="breadcrumb">
			<span>

			</span>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a href="{{ url('/') }}" rel="v:url" property="v:title" class="home">
					Home
				</a>
			</div>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a title="Contacto" rel="v:url" property="v:title">
					Contacto
				</a>
			</div>
		</div>

		<h2 class="h2-categories">
			<span>
				Contacto
			</span>
		</h2>
	</header>



	<div class="cat-layout clearfix page-contact">
		<div class="content-contact">
			<div class="row">
				<div class="col-md-12">
					<br>

					<p>
						Si tienes cualquier consulta, sugerencia o aportación no dudes en hacérnoslo saber. Para ello tenemos a total disposición nuestro correo <a href="mailto:info@evaluacionpsicologica.es">info@evaluacionpsicologica.es</a> o a través del siguiente formulario. Nos pondremos en contacto con usted, tán brevemente como nos sea posible.
					</p>
				</div>
				<div id="form-contact">
					<br>
					@include('components.form-contact', ['section'=>'contact-page'])
					<br>
				</div>
				<div class="col-md-12">
					<div id="map-contact"></div>
					<div id="submap-contact">
						<p>Evaluación Psicológica, Av. de Cervantes nº2, 29016 Málaga</p>
					</div>
				</div>
			</div>

			<br>
			<br>
			<br>
			<br>
		</div>
	</div>
</div>

@endsection