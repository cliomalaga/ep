@extends('template.master')

@section('title')
| Buscador
@endsection

@section('sidebar')
@include('page.sidebar-page')
@endsection



@section('content')
<div class="content-container">
	<header>
		<div class="breadcrumb">
			<span>

			</span>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a href="{{ url('/') }}" rel="v:url" property="v:title" class="home">
					Home
				</a>
			</div>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a title="Buscador" rel="v:url" property="v:title">
					Buscador
				</a>
			</div>
		</div>

		<h2 class="h2-categories">
			<span>
				Buscador
			</span>
		</h2>
	</header>



	<div class="cat-layout clearfix page-search">
		<div class="row">
			<div class="col-md-12">
				<form id="search-form" class="search-form" method="POST" action="buscador">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					@if(!empty($search))
					<input type="text" id="input-search-page" name="search" placeholder="Buscar ..." value="{{ $search }}">
					@else
					<input type="text" id="input-search-page" name="search" placeholder="Buscar ..." value="">
					@endif

					<button id="button-search-menu">
						<span class="glyphicon glyphicon-search"></span>
					</button>
				</form>
			</div>


			<div class="col-md-12">

				@if(!empty($items))
				@foreach($items as $item)
				<article class="def def-medium">
					<div class="entry">
						<div class="entry-meta">
							<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
						</div>
						<h3 itemprop="name">
							<a itemprop="url" href="{{ url($item->url) }}">
								{!! ucfirst(str_limit($item->title, $limit = 100, $end = '...')) !!}
							</a>
						</h3>
						<div class="subtitle">
							{!! ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) !!}
						</div>
						<div class="entry-meta">
							<span itemprop="categories" class="entry-categories">
								<a href="{{ url($item->category->url) }}" title="{{ ucfirst($item->category->name) }}">
									{{ ucfirst($item->category->name) }}
								</a>
								|
								<a href="{{ url($item->subcategory->url) }}" title="{{ ucfirst($item->subcategory->name) }}">
									{{ ucfirst($item->subcategory->name) }}
								</a>
							</span>
							<span itemprop="author" class="entry-author">
								<a href="{{ url($item->created_by->url) }}">
									{{ ucwords(mb_strtolower($item->created_by->name)) }} {{ ucwords(mb_strtolower($item->created_by->lastname)) }}
								</a>
							</span>
						</div>
						<div class="text">
							<p>
								{!! ucfirst(str_limit($item->text, $limit = 200, $end = '...')) !!}
							</p>
						</div>

					</div>
					<figure class="overlay relative">
						<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
							<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
						</a>
						<figcaption>
							<div class="entry-meta"></div>
						</figcaption>
					</figure>
				</article>
				@endforeach

				@else


				@if(!empty($searchPage))
				@include('components.empty-search')
				@endif


				@endif


			</div>
		</div>

		<br>
		<br>
		<br>
		<br>
	</div>
</div>

@endsection