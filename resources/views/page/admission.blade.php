@extends('template.master')

@section('title')
| Admisión de Socios
@endsection

@section('sidebar')
@include('page.sidebar-page')
@endsection



@section('content')
<div class="content-container">
	<header>
		<div class="breadcrumb">
			<span>

			</span>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a href="{{ url('/') }}" rel="v:url" property="v:title" class="home">
					Home
				</a>
			</div>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a title="Admisión de Socios" rel="v:url" property="v:title">
					Admisión de Socios
				</a>
			</div>
		</div>

		<h2 class="h2-categories">
			<span>
				Admisión de Socios
			</span>
		</h2>
	</header>



	<div class="cat-layout clearfix page-static">
		<div class="row">
			<div class="col-md-12">

				<br>

				<h2>
					Documento de domiciliación de cuotas a SEAEP
				</h2>

				<p>
					Una vez solicitada la admisión en la Sociedad Española para el Avance de la Evaluación Psicológica, se ha de bajar y rellenar el impreso adjunto, para la domiciliación de las cuotas anuales (100 €) de cada socio. Debe enviarse al correo electrónico de la sociedad SEAEP2@gmail.com.También se pueden realizar los ingresos directamente en la cuenta bancaria de SEAEP  (IBAN ES29-0049-5167-91-2116346499)
				</p>

				<br>
				<a href="http://evaluacionpsicologica.es/dashboard/public/attacheds/documents/20171221_SEAEP_Impreso_domiciliacion.doc" download="" target="_self" title="Descargar adjunto" alt="Descargar adjunto">
					<i class="fa fa-file"></i>
					SEAEP_Impreso_domiciliacion.doc
				</a>

				<br>
				<br>
				<br>
				<br>
			</div>
		</div>
	</div>
</div>

@endsection