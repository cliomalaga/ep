@extends('template.master')

@section('title')
| Creative Commons
@endsection

@section('sidebar')
@include('page.sidebar-page')
@endsection



@section('content')
<div class="content-container">
	<header>
		<div class="breadcrumb">
			<span>

			</span>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a href="{{ url('/') }}" rel="v:url" property="v:title" class="home">
					Home
				</a>
			</div>
			<div class="vbreadcrumb" typeof="v:Breadcrumb">
				<a title="Creative Commons" rel="v:url" property="v:title">
					Creative Commons
				</a>
			</div>
		</div>

		<h2 class="h2-categories">
			<span>
				Creative Commons
			</span>
		</h2>
	</header>



	<div class="cat-layout clearfix page-static">
		<div class="row">
			<div class="col-md-12">

				<h2>
					¿Qué es Creative Commons? - Wikipedia
				</h2>
				<p>
					Creative Commons (CC) ―en español, "Comunes Creativos"― es una organización sin fines de lucro dedicada a promover el acceso y el intercambio de cultura. Desarrolla un conjunto de instrumentos jurídicos de carácter gratuito que facilitan usar y compartir tanto la creatividad como el conocimiento. Su sede central se encuentra en Mountain View, en el estado de California, Estados Unidos.
				</p>

				<p>
					Los instrumentos jurídicos desarrollados por la organización consisten en un conjunto de “modelos de contratos de licenciamiento” o licencias de derechos de autor (licencias Creative Commons o licencias CC) que ofrecen al autor de una obra una manera simple y estandarizada de otorgar permiso al público para compartir y usar su trabajo creativo bajo los términos y condiciones de su elección. En este sentido, las licencias Creative Commons permiten al autor cambiar fácilmente los términos y condiciones de derechos de autor de su obra de “todos los derechos reservados” a “algunos derechos reservados”.
				</p>

				<p>
					Las licencias Creative Commons no reemplazan a los derechos de autor, sino que se apoyan en estos para permitir elegir los términos y condiciones de la licencia de una obra de la manera que mejor satisfaga al titular de los derechos. Por tal motivo, estas licencias han sido entendidas por muchos como una manera en que los autores pueden tomar el control de cómo quieren compartir su propiedad intelectual.
				</p>

				<p>
					La organización fue fundada en 2001 por Lawrence Lessig (exprofesor de derecho de la Universidad de Stanford y especialista en ciberderecho), Hal Abelson y Eric Eldred2 con el soporte del Center for the Public Domain. El primer artículo acerca de Creative Commons en una publicación de interés general fue escrito por Hal Plotkin en febrero de 2002. El primer conjunto de licencias de copyright fue lanzado en diciembre de 2002. En 2008, había estimados unos 130 millones de trabajos bajo licencias Creative Commons. En octubre de 2011, solo Flickr albergaba más de 200 millones de fotografías con licencias Creative Commons. A fines de 2015 había más de 1.100 millones de trabajos bajo licencias Creative Commons en todo el mundo.
				</p>

				<p>
					Creative Commons está dirigida por una junta directiva y un consejo consultivo. Cuenta además con una red mundial de más de 100 organizaciones afiliadas trabajando en más de 85 países.
				</p>

				<br>

				<h2>
					Licencias Creative Commons
				</h2>

				<p>
					Las licencias Creative Commons (CC) están al momento disponibles en 43 jurisdicciones diferentes de todo el mundo, junto con otras 19 que se están desarrollando. Las licencias para jurisdicciones fuera de los Estados Unidos se hallan bajo la competencia de Creative Commons International.
				</p>

				<p>
					La última versión de las licencias Creative Commons es la 4.0 (2016), en la cual se ha ampliado la cobertura legal también para bases de datos y se ha mejorado la legibilidad, entre otras cosas.
				</p>

				<br>

				<h2>
					Tipo de Licencias
				</h2>

				<p>
					Muchas de las licencias,y en particular todas las licencias originales, conceden ciertos "derechos básicos" (baseline rights), como el derecho a distribuir la obra con copyright, sin cargo. Algunas de las licencias más nuevas no conceden estos derechos. Los términos de cada licencia dependen de cuatro condiciones.
				</p>

				<br>

				<h2>
					Condiciones
				</h2>

				<p>
					<b>Atribución (BY)</b>
					El beneficiario de la licencia tiene el derecho de copiar, distribuir, exhibir y representar la obra y hacer obras derivadas siempre y cuando reconozca y cite la obra de la forma especificada por el autor o el licenciante.
				</p>

				<p>
					<b>No Comercial (NC)</b>
					El beneficiario de la licencia tiene el derecho de copiar, distribuir, exhibir y representar la obra y hacer obras derivadas para fines no comerciales.
				</p>

				<p>
					<b>No Derivadas (ND)</b>
					El beneficiario de la licencia solamente tiene el derecho de copiar, distribuir, exhibir y representar copias literales de la obra y no tiene el derecho de producir obras derivadas.
				</p>

				<p>
					<b>Compartir Igual (SA)</b>
					El beneficiario de la licencia tiene el derecho de distribuir obras derivadas bajo una licencia idéntica a la licencia que regula la obra original.
				</p>

				<br>
				<br>
				<br>
				<br>

			</div>
		</div>
	</div>
</div>

@endsection