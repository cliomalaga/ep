<form class="form-contact" method="POST" action="logueandose">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	@if (Session::has('login-response'))
	<h1 class="response">{{ Session::get('login-response') }}</h1>
	<br>
	@endif

	<label>Email:</label>
	<input class="field" type="email" name="email" required="">

	<label>Contraseña:</label>
	<input class="field" type="password" name="password" required="">

	<br>

	<input class="buy-button ss-button" type="submit" value="Acceder">
</form>