@inject('functions', 'App\Repositories\Functions\FunctionsRepository')

@php
$randoms = $functions->getRandomHome();
@endphp

@if(!empty($randoms))
<header>
	<h2>OTROS USUARIOS ESTAN VIENDO</h2>
	<span class="borderline"></span>
</header>
<div class="articles relative">
	<div class="row">
		@foreach($randoms as $item)
		<div class="col-sm-4 shadow-ver-right has-header">
			<div class="shadow-box">
				<article class="def">
					<figure class="overlay relative">
						<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
							<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
						</a>
						<figcaption>
							<div class="entry-meta"></div>
						</figcaption>
					</figure>
					<div class="entry">
						<span class="entry-category parent-cat-3 cat-3">
							<a itemprop="url" href="{{ url($item->category->url) }}">{{ ucwords(mb_strtolower($item->category->name)) }}</a>
							-
							<a itemprop="url" href="{{ url($item->subcategory->url) }}">{{ ucwords(mb_strtolower($item->subcategory->name)) }}</a>
						</span>
						<h3 itemprop="name">
							<a itemprop="url" href="{{ url($item->url) }}">
								{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
							</a>
						</h3>
						<div class="entry-meta">
							<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
						</div>
						<div class="text hidden-xs">
							{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
						</div>
					</div>
				</article>
			</div>
		</div>
		@endforeach
	</div>
</div>
@endif