<div id="cookies-bar">
	<div id="infobox">
		<p>Esta web utiliza cookies para obtener datos estadísticos de la navegación de sus usuarios. Si continúas navegando consideramos que aceptas su uso.
			<a href="{{ url('politica-cookies') }}">Mas información</a>
			<a class="btn-accept" onclick="aceptar_cookies();">Aceptar</a>
		</p>
	</div>
</div>