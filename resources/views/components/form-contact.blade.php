<form class="form-contact" method="POST" action="enviandomensaje">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="section" value="{{ $section }}">

	@if ($section=='contact-aside' && Session::has('contact-aside'))
	<h1 class="response">{{ Session::get('contact-aside') }}</h1>
	<br>
	@endif
	@if ($section=='contact-page' && Session::has('contact-page'))
	<h1 class="response">{{ Session::get('contact-page') }}</h1>
	<br>
	@endif

	<label>Nombre:</label>
	<input class="field" type="text" name="name" required="" value="{{ old('name') }}">

	<label>Email:</label>
	<input class="field" type="email" name="email" required="" value="{{ old('email') }}">

	<label>Mensaje:</label>
	<textarea class="field" name="messagge">{{ old('messagge') }}</textarea>

	<br>

	<input class="buy-button ss-button" type="submit" value="Enviar mensaje">
</form>