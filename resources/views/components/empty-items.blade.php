<div class="row">
	<div class="container-404">
		<h3>NO EXISTEN ARTÍCULOS EN ESTA SECCIÓN</h3>
		<h5>
			Si quiere enviarnos sugerencias o aportaciones, por favor <a href="{{ url('contacto') }}">contacte</a> con nosotros y rellenaremos esta sección tán pronto como nos sea posible.
		</h5>
		<img itemprop="image" src="{{resources('images/ep/404.png')}}" class="img-responsive">
	</div>
</div>


<!-- Other users viewed -->
<aside id="others-viewed" class="posts-related loop-cat standard loop-cat-3">
	@include('components.others-viewed')
</aside>