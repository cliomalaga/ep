<div class="row">
	<div class="container-404 ">
		<h3>NO HAY COINCIDENCIAS</h3>
		<h5>
			Asegúrese que la búsqueda esté escrita correctamente. Si necesita ayuda, por favor <a href="{{ url('contacto') }}">contacte</a> con nosotros y le responderemos tán pronto como nos sea posible.
		</h5>
		<img itemprop="image" src="{{resources('images/ep/404.png')}}" class="img-responsive">
	</div>
</div>
