<!--More content -->


<!--BLOCK HEAD-->
<section class="section-three section-full clearfix">
	<aside id="mip-ajax-block-59357b8e008fa" data-block="block-10" data-cat="7" data-count="2" data-max-pages="10" data-offset="3" data-tag="" data-sort="date" data-display="root" data-img-format-1="miptheme-post-thumb-4" data-img-format-2="" data-img-width-1="4100" data-img-width-2="" data-img-height-1="320" data-img-height-2="" data-text="0" data-meta="">
		<div id="top-content" class="articles relative clearfix">
			<div class="row">


				<!-- ARTICLE TOP 1 -->
				@if(!empty($items->test[1]))
				@php
				$item = $items->test[1];
				@endphp
				<div class="col-md-6 shadow-top-left">
					<article class="def def-medium def-overlay item-count-0">
						<figure class="overlay relative">
							<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay">
								<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
							</a>
							<figcaption>
								<div class="entry">
									<h3 itemprop="name">
										<a itemprop="url" href="{{ url($item->url) }}">
											{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
										</a>
									</h3>
									<span class="entry-category parent-cat-1 cat-1">
										<a itemprop="url" href="{{ url($item->category->url) }}">PRUEBAS</a>
									</span>
									<div class="post-meta text-center">
										<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
									</div>
								</div>
							</figcaption>
						</figure>
					</article>
				</div>
				@endif

				<!-- ARTICLE TOP 2 -->
				@if(!empty($items->faq[1]))
				@php
				$item = $items->faq[1];
				@endphp
				<div class="col-md-6 shadow-top-left">
					<article class="def def-medium def-overlay item-count-0">
						<figure class="overlay relative">
							<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay">
								<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
							</a>
							<figcaption>
								<div class="entry">
									<h3 itemprop="name">
										<a itemprop="url" href="{{ url($item->url) }}">
											{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
										</a>
									</h3>
									<span class="entry-category parent-cat-1 cat-1">
										<a itemprop="url" href="{{ url($item->category->url) }}">PREGUNTAS</a>
									</span>
									<div class="post-meta text-center">
										<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
									</div>
								</div>
							</figcaption>
						</figure>
					</article>
				</div>
				@endif
			</div>


			<div class="row">
				<div class="col-md-12 has-header">
					<aside id="home-content" data-block="block-01-right" data-cat="" data-count="4" data-max-pages="11" data-offset="0" data-tag="" data-sort="date" data-display="root" data-img-format-1="miptheme-post-thumb-3" data-img-format-2="miptheme-post-thumb-7" data-img-width-1="577" data-img-width-2="176" data-img-height-1="394" data-img-height-2="120" data-text="100" data-meta="4">
						<div class="articles relative clearfix">


							<!-- ARTICLE 3 -->
							@if(!empty($items->teaching[1]))
							@php
							$item = $items->teaching[1];
							@endphp
							<div class="shadow-box shadow-top-left clearfix">
								<article class="def def-small">
									<div class="row clearfix">
										<div class="col-sm-4">
											<figure class="overlay relative">
												<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
													<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
												</a>
												<figcaption>
													<div class="entry-meta"></div>
												</figcaption>
											</figure>
										</div>
										<div class="col-sm-8 no-left">
											<span class="entry-category parent-cat-3 cat-3">
												<a itemprop="url" href="{{ url($item->category->url) }}">ENSEÑANZAS</a>
											</span>
											<h3 itemprop="name">
												<a itemprop="url" href="{{ url($item->url) }}">
													{{  ucfirst(str_limit($item->title, $limit = 70, $end = '')) }}
												</a>
											</h3>
											<div class="text">
												{{ ucfirst(str_limit($item->text, $limit = 250, $end = '...')) }}
											</div>
											<br>
											<div class="entry-meta">
												<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
											</div>
										</div>
									</div>
								</article>
							</div>
							@endif

							<!-- ARTICLE 4 -->
							@if(!empty($items->casee[1]))
							@php
							$item = $items->casee[1];
							@endphp
							<div class="shadow-box shadow-top-left clearfix">
								<article class="def def-small">
									<div class="row clearfix">
										<div class="col-sm-4">
											<figure class="overlay relative">
												<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
													<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
												</a>
												<figcaption>
													<div class="entry-meta"></div>
												</figcaption>
											</figure>
										</div>
										<div class="col-sm-8 no-left">
											<span class="entry-category parent-cat-3 cat-3">
												<a itemprop="url" href="{{ url($item->category->url) }}">CASOS</a>
											</span>
											<h3 itemprop="name">
												<a itemprop="url" href="{{ url($item->url) }}">
													{{  ucfirst(str_limit($item->title, $limit = 70, $end = '')) }}
												</a>
											</h3>
											<div class="text">
												{{ ucfirst(str_limit($item->text, $limit = 250, $end = '...')) }}
											</div>
											<br>
											<div class="entry-meta">
												<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
											</div>
										</div>
									</div>
								</article>
							</div>
							@endif

							<!-- ARTICLE 5 -->
							@if(!empty($items->investigation[1]))
							@php
							$item = $items->investigation[1]
							@endphp
							<div class="shadow-box shadow-top-left clearfix">
								<article class="def def-small">
									<div class="row clearfix">
										<div class="col-sm-4">
											<figure class="overlay relative">
												<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
													<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
												</a>
												<figcaption>
													<div class="entry-meta"></div>
												</figcaption>
											</figure>

										</div>
										<div class="col-sm-8 no-left">
											<span class="entry-category parent-cat-3 cat-3">
												<a itemprop="url" href="{{ url($item->category->url) }}">INVESTIGACIONES</a>
											</span>
											<h3 itemprop="name">
												<a itemprop="url" href="{{ url($item->url) }}">
													{{ ucfirst(str_limit($item->title, $limit = 70, $end = '')) }}
												</a>
											</h3>
											<div class="text">
												{{ ucfirst(str_limit($item->text, $limit = 250, $end = '...')) }}
											</div>
											<br>
											<div class="entry-meta">
												<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
											</div>
										</div>
									</div>
								</article>
							</div>
							@endif

						</div>
					</aside>
				</div>
			</div>
		</div>
	</aside>
</section>




<!--BANNER ADS-->
<!--								<section>
	<div class="row ad">
		<a href="http://sportsland.newsthemes.info/" target="_blank">
			<img src="{{resources('images/template/banner_728_sportsland.jpg')}}" width="728" alt="">
		</a>
	</div>
</section>-->


<!-- BLOCK FAQS -->
<section id="block-faqs" class="section-three section-full clearfix" data-page="1">
	<aside id="mip-ajax-block-59357b8e483e3" >
		<header>
			<h2>PREGUNTAS</h2>
			<span class="borderline"></span>
			<div class="paging mip-ajax-nav ajax-nav-header">
				<a class="prev disabled"></a>
				<a class="next"></a>
			</div>
		</header>
		<!--<br>-->
		<div class="articles relative clearfix">
			<div class="row">
				@foreach($faqs as $item)
				<div class="col-sm-4">
					<div class="shadow-box">
						<article class="def">
							<figure class="overlay relative">
								<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
									<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
								</a>
								<figcaption>
									<div class="entry-meta"></div>
								</figcaption>
							</figure>
							<div class="entry">
								<span class="entry-category parent-cat-1 cat-1">
									<a itemprop="url" href="{{ url($item->subcategory->url) }}">
										{{$item->subcategory->url}}
									</a>
								</span>
								<h3 itemprop="name">
									<a itemprop="url" href="{{ url($item->url) }}">
										{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
									</a>
								</h3>
								<div class="entry-meta">
									<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
								</div>
							</div>
						</article>
					</div>
				</div>
				@endforeach


			</div>
		</div>
	</aside>
</section>




<!-- BLOCK ONE TESTS -->
<section class="three-articles section-two section-full section-border clearfix">
	<aside id="mip-ajax-block-59357b8e34d87" >
		<header>
			<h2>PRUEBAS</h2>
			<span class="borderline"></span>
		</header>
		<div class="row">
			<div class="articles relative clearfix">
				@if(!empty($items->test[2]))
				@php
				$item = $items->test[2]
				@endphp
				<div class="col-sm-4 shadow-ver-right">
					<div class="shadow-box shadow-top-left">
						<article class="def">
							<figure class="overlay relative">
								<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
									<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
								</a>
								<figcaption>
									<div class="entry-meta"></div>
								</figcaption>
							</figure>
							<div class="entry">
								<span class="entry-category parent-cat-1 cat-1">
									<a itemprop="url" href="{{ url($item->subcategory->url) }}">
										{{ $item->subcategory->name }}
									</a>
								</span>
								<h3 itemprop="name">
									<a itemprop="url" href="{{ url($item->url) }}">
										{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
									</a>
								</h3>
								<div class="entry-meta">
									<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
								</div>
								<div class="text">
									{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
								</div>
							</div>
						</article>
					</div>
				</div>
				@endif



				<div class="col-sm-8">

					@if(!empty($items->test[3]))
					@php
					$item = $items->test[3]
					@endphp
					<div class="shadow-box shadow-top-left clearfix">
						<article class="def def-small">
							<div class="row clearfix">
								<div class="col-xs-5">
									<figure class="overlay relative">
										<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
											<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
										</a>
										<figcaption>
											<div class="entry-meta"></div>
										</figcaption>
									</figure>
								</div>
								<div class="col-xs-7 no-left no-left">
									<span class="entry-category parent-cat-1 cat-1">
										<a itemprop="url" href="{{ url($item->subcategory->url) }}">
											{{ $item->subcategory->name }}
										</a>
									</span>
									<h3 itemprop="name">
										<a itemprop="url" href="{{ url($item->url) }}">
											{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
										</a>
									</h3>
									<div class="text">
										{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
									</div>
									<div class="entry-meta">
										<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
									</div>
								</div>
							</div>
						</article>
					</div>
					@endif

					@if(!empty($items->test[4]))
					@php
					$item = $items->test[4]
					@endphp
					<div class="shadow-box shadow-top-left clearfix">
						<article class="def def-small">
							<div class="row clearfix">
								<div class="col-xs-5">
									<figure class="overlay relative">
										<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
											<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
										</a>
										<figcaption>
											<div class="entry-meta"></div>
										</figcaption>
									</figure>
								</div>
								<div class="col-xs-7 no-left no-left">
									<span class="entry-category parent-cat-1 cat-1">
										<a itemprop="url" href="{{ url($item->category->url.'/'.sluggable($item->subcategory->name )) }}">
											{{ $item->subcategory->name }}
										</a>
									</span>
									<h3 itemprop="name">
										<a itemprop="url" href="{{ url($item->url) }}">
											{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
										</a>
									</h3>
									<div class="text">
										{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
									</div>
									<div class="entry-meta">
										<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
									</div>
								</div>
							</div>
						</article>
					</div>
					@endif
				</div>
			</div>
		</div>
	</aside>
</section>








<!-- BLOCK TWO TEACHINGS -->
<section class="three-articles section-two section-full section-border clearfix">
	<aside id="mip-ajax-block-59357b8e34d87" >
		<header>
			<h2>ENSEÑANZAS</h2>
			<span class="borderline"></span>
		</header>
		<div class="row">
			<div class="articles relative clearfix">
				@if(!empty($items->teaching[2]))
				@php
				$item = $items->teaching[2]
				@endphp
				<div class="col-sm-4 shadow-ver-right">
					<div class="shadow-box shadow-top-left">
						<article class="def">
							<figure class="overlay relative">
								<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
									<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
								</a>
								<figcaption>
									<div class="entry-meta"></div>
								</figcaption>
							</figure>
							<div class="entry">
								<span class="entry-category parent-cat-1 cat-1">
									<a itemprop="url" href="{{ url($item->subcategory->url) }}">
										{{ $item->subcategory->name }}
									</a>
								</span>
								<h3 itemprop="name">
									<a itemprop="url" href="{{ url($item->url) }}">
										{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
									</a>
								</h3>
								<div class="entry-meta">
									<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
								</div>
								<div class="text">
									{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
								</div>
							</div>
						</article>
					</div>
				</div>
				@endif



				<div class="col-sm-8">

					@if(!empty($items->teaching[3]))
					@php
					$item = $items->teaching[3]
					@endphp
					<div class="shadow-box shadow-top-left clearfix">
						<article class="def def-small">
							<div class="row clearfix">
								<div class="col-xs-5">
									<figure class="overlay relative">
										<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
											<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
										</a>
										<figcaption>
											<div class="entry-meta"></div>
										</figcaption>
									</figure>
								</div>
								<div class="col-xs-7 no-left no-left">
									<span class="entry-category parent-cat-1 cat-1">
										<a itemprop="url" href="{{ url($item->subcategory->url) }}">
											{{ $item->subcategory->name }}
										</a>
									</span>
									<h3 itemprop="name">
										<a itemprop="url" href="{{ url($item->url) }}">
											{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
										</a>
									</h3>
									<div class="text">
										{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
									</div>
									<div class="entry-meta">
										<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
									</div>
								</div>
							</div>
						</article>
					</div>
					@endif

					@if(!empty($items->teaching[4]))
					@php
					$item = $items->teaching[4]
					@endphp
					<div class="shadow-box shadow-top-left clearfix">
						<article class="def def-small">
							<div class="row clearfix">
								<div class="col-xs-5">
									<figure class="overlay relative">
										<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
											<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
										</a>
										<figcaption>
											<div class="entry-meta"></div>
										</figcaption>
									</figure>
								</div>
								<div class="col-xs-7 no-left no-left">
									<span class="entry-category parent-cat-1 cat-1">
										<a itemprop="url" href="{{ url( $item->subcategory->url) }}">
											{{ $item->subcategory->name }}
										</a>
									</span>
									<h3 itemprop="name">
										<a itemprop="url" href="{{ url($item->url) }}">
											{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
										</a>
									</h3>
									<div class="text">
										{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
									</div>
									<div class="entry-meta">
										<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
									</div>
								</div>
							</div>
						</article>
					</div>
					@endif
				</div>
			</div>
		</div>
	</aside>
</section>





<!-- BLOCK THREE CASES -->
<section class="three-articles section-two section-full section-border clearfix">
	<aside id="mip-ajax-block-59357b8e34d87" >
		<header>
			<h2>CASOS</h2>
			<span class="borderline"></span>
		</header>
		<div class="row">
			<div class="articles relative clearfix">
				@if(!empty($items->casee[2]))
				@php
				$item = $items->casee[2]
				@endphp
				<div class="col-sm-4 shadow-ver-right">
					<div class="shadow-box shadow-top-left">
						<article class="def">
							<figure class="overlay relative">
								<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
									<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
								</a>
								<figcaption>
									<div class="entry-meta"></div>
								</figcaption>
							</figure>
							<div class="entry">
								<span class="entry-category parent-cat-1 cat-1">
									<a itemprop="url" href="{{ url($item->subcategory->url) }}">
										{{ $item->subcategory->name }}
									</a>
								</span>
								<h3 itemprop="name">
									<a itemprop="url" href="{{ url($item->url) }}">
										{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
									</a>
								</h3>
								<div class="entry-meta">
									<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
								</div>
								<div class="text">
									{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
								</div>
							</div>
						</article>
					</div>
				</div>
				@endif



				<div class="col-sm-8">

					@if(!empty($items->casee[3]))
					@php
					$item = $items->casee[3]
					@endphp
					<div class="shadow-box shadow-top-left clearfix">
						<article class="def def-small">
							<div class="row clearfix">
								<div class="col-xs-5">
									<figure class="overlay relative">
										<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
											<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
										</a>
										<figcaption>
											<div class="entry-meta"></div>
										</figcaption>
									</figure>
								</div>
								<div class="col-xs-7 no-left no-left">
									<span class="entry-category parent-cat-1 cat-1">
										<a itemprop="url" href="{{ url($item->subcategory->url) }}">
											{{ $item->subcategory->name }}
										</a>
									</span>
									<h3 itemprop="name">
										<a itemprop="url" href="{{ url($item->url) }}">
											{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
										</a>
									</h3>
									<div class="text">
										{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
									</div>
									<div class="entry-meta">
										<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
									</div>
								</div>
							</div>
						</article>
					</div>
					@endif

					@if(!empty($items->casee[4]))
					@php
					$item = $items->casee[4]
					@endphp
					<div class="shadow-box shadow-top-left clearfix">
						<article class="def def-small">
							<div class="row clearfix">
								<div class="col-xs-5">
									<figure class="overlay relative">
										<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
											<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
										</a>
										<figcaption>
											<div class="entry-meta"></div>
										</figcaption>
									</figure>
								</div>
								<div class="col-xs-7 no-left no-left">
									<span class="entry-category parent-cat-1 cat-1">
										<a itemprop="url" href="{{ url($item->subcategory->url) }}">
											{{ $item->subcategory->name }}
										</a>
									</span>
									<h3 itemprop="name">
										<a itemprop="url" href="{{ url($item->url) }}">
											{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
										</a>
									</h3>
									<div class="text">
										{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
									</div>
									<div class="entry-meta">
										<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
									</div>
								</div>
							</div>
						</article>
					</div>
					@endif
				</div>
			</div>
		</div>
	</aside>
</section>




<!-- BLOCK FOUR INVESTIGATIONS -->
<section class="three-articles section-two section-full section-border clearfix">
	<aside id="mip-ajax-block-59357b8e34d87" >
		<header>
			<h2>INVESTIGACIONES</h2>
			<span class="borderline"></span>
		</header>
		<div class="row">
			<div class="articles relative clearfix">
				@if(!empty($items->investigation[2]))
				@php
				$item = $items->investigation[2]
				@endphp
				<div class="col-sm-4 shadow-ver-right">
					<div class="shadow-box shadow-top-left">
						<article class="def">
							<figure class="overlay relative">
								<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
									<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
								</a>
								<figcaption>
									<div class="entry-meta"></div>
								</figcaption>
							</figure>
							<div class="entry">
								<span class="entry-category parent-cat-1 cat-1">
									<a itemprop="url" href="{{ url($item->subcategory->url) }}">
										{{ $item->subcategory->name }}
									</a>
								</span>
								<h3 itemprop="name">
									<a itemprop="url" href="{{ url($item->url) }}">
										{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
									</a>
								</h3>
								<div class="entry-meta">
									<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
								</div>
								<div class="text">
									{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
								</div>
							</div>
						</article>
					</div>
				</div>
				@endif



				<div class="col-sm-8">

					@if(!empty($items->investigation[3]))
					@php
					$item = $items->investigation[3]
					@endphp
					<div class="shadow-box shadow-top-left clearfix">
						<article class="def def-small">
							<div class="row clearfix">
								<div class="col-xs-5">
									<figure class="overlay relative">
										<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
											<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
										</a>
										<figcaption>
											<div class="entry-meta"></div>
										</figcaption>
									</figure>
								</div>
								<div class="col-xs-7 no-left no-left">
									<span class="entry-category parent-cat-1 cat-1">
										<a itemprop="url" href="{{ url($item->subcategory->url) }}">
											{{ $item->subcategory->name }}
										</a>
									</span>
									<h3 itemprop="name">
										<a itemprop="url" href="{{ url($item->url) }}">
											{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
										</a>
									</h3>
									<div class="text">
										{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
									</div>
									<div class="entry-meta">
										<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
									</div>
								</div>
							</div>
						</article>
					</div>
					@endif

					@if(!empty($items->investigation[4]))
					@php
					$item = $items->investigation[4]
					@endphp
					<div class="shadow-box shadow-top-left clearfix">
						<article class="def def-small">
							<div class="row clearfix">
								<div class="col-xs-5">
									<figure class="overlay relative">
										<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay-small">
											<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ ucfirst($item->title) }}" class="img-responsive">
										</a>
										<figcaption>
											<div class="entry-meta"></div>
										</figcaption>
									</figure>
								</div>
								<div class="col-xs-7 no-left no-left">
									<span class="entry-category parent-cat-1 cat-1">
										<a itemprop="url" href="{{ url($item->subcategory->url) }}">
											{{ $item->subcategory->name }}
										</a>
									</span>
									<h3 itemprop="name">
										<a itemprop="url" href="{{ url($item->url) }}">
											{{ ucfirst(str_limit($item->title, $limit = 100, $end = '...')) }}
										</a>
									</h3>
									<div class="text">
										{{ ucfirst(str_limit($item->subtitle, $limit = 100, $end = '...')) }}
									</div>
									<div class="entry-meta">
										<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
									</div>
								</div>
							</div>
						</article>
					</div>
					@endif
				</div>
			</div>
		</div>
	</aside>
</section>




<!-- Other users viewed -->
<section id="others-viewed" class="home-others-viewed last-section section-two section-full col-style-one clearfix">
	<aside id="mip-ajax-block-58f10128045b2">
		@include('components.others-viewed')
	</aside>
</section>


<br>



