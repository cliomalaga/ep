<!-- Top header -->
<div id="top-grid" class="top-grid-layout-1  clearfix">
	<div class="row">


		@if(!empty($items->test[0]))
		@php
		$item = $items->test[0];
		@endphp
		<div class="col-xs-12 col-md-6">
			<article class="def def-medium def-overlay item-count-1">
				<figure class="overlay relative">
					<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay">
						<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ $item->title }}" class="img-responsive">
					</a>
					<figcaption>
						<div class="entry">
							<h3 itemprop="name">
								<a itemprop="url" href="{{ url($item->url) }}">{{ $item->title }}</a>
							</h3>
							<span class="entry-category parent-cat-1 cat-1">
								<a itemprop="url" href="{{ url($item->category->url) }}">PRUEBAS</a>
							</span>
							<div class="post-meta text-center">
								<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
							</div>
						</div>
					</figcaption>
				</figure>
			</article>
		</div>
		@endif


		<div class="col-md-3">
			@if(!empty($items->faq[0]))
			@php
			$item = $items->faq[0];
			@endphp
			<article class="def def-medium def-overlay item-count-2">
				<figure class="overlay relative">
					<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay">
						<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ $item->title }}" class="img-responsive">
					</a>
					<figcaption>
						<div class="entry">
							<h3 itemprop="name">
								<a itemprop="url" href="{{ url($item->url) }}">{{ $item->title }}</a>
							</h3>
							<span class="entry-category parent-cat-1 cat-1">
								<a itemprop="url" href="{{ url($item->category->url) }}">PREGUNTAS</a>
							</span>
							<div class="post-meta text-center">
								<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
							</div>
						</div>
					</figcaption>
				</figure>
			</article>
			@endif


			@if(!empty($items->teaching[0]))
			@php
			$item = $items->teaching[0];
			@endphp
			<article class="def def-medium def-overlay item-count-3">
				<figure class="overlay relative">
					<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay">
						<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ $item->title }}" class="img-responsive">
					</a>
					<figcaption>
						<div class="entry">
							<h3 itemprop="name">
								<a itemprop="url" href="{{ url($item->url) }}">{{ $item->title }}</a>
							</h3>
							<span class="entry-category parent-cat-1 cat-1">
								<a itemprop="url" href="{{ url($item->category->url) }}">ENSEÑANZAS</a>
							</span>
							<div class="post-meta text-center">
								<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
							</div>
						</div>
					</figcaption>
				</figure>
			</article>
			@endif
		</div>



		<div class="col-md-3">

			@if(!empty($items->casee[0]))
			@php
			$item = $items->casee[0];
			@endphp
			<article class="def def-medium def-overlay item-count-4">
				<figure class="overlay relative">
					<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay">
						<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ $item->title }}" class="img-responsive">
					</a>
					<figcaption>
						<div class="entry">
							<h3 itemprop="name">
								<a itemprop="url" href="{{ url($item->url) }}">{{ $item->title }}</a>
							</h3>
							<span class="entry-category parent-cat-1 cat-1">
								<a itemprop="url" href="{{ url($item->category->url) }}">CASOS</a>
							</span>
							<div class="post-meta text-center">
								<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
							</div>
						</div>
					</figcaption>
				</figure>
			</article>
			@endif


			@if(!empty($items->investigation[0]))
			@php
			$item = $items->investigation[0];
			@endphp
			<article class="def def-medium def-overlay item-count-5">
				<figure class="overlay relative">
					<a itemprop="url" href="{{ url($item->url) }}" class="thumb-overlay">
						<img itemprop="image" src="{{urlDashboard($item->image)}}" alt="{{ $item->title }}" class="img-responsive">
					</a>
					<figcaption>
						<div class="entry">
							<h3 itemprop="name">
								<a itemprop="url" href="{{ url($item->url) }}">{{ $item->title }}</a>
							</h3>
							<span class="entry-category parent-cat-1 cat-1">
								<a itemprop="url" href="{{ url($item->category->url) }}">INVESTIGACIONES</a>
							</span>
							<div class="post-meta text-center">
								<time class="entry-date" datetime="{{ $item->created_at }}" itemprop="dateCreated">{{ strftime('%d %B, %Y', strtotime($item->created_at)) }}</time>
							</div>
						</div>
					</figcaption>
				</figure>
			</article>
			@endif

		</div>
	</div>
</div>