@extends('template.master')

@section('title')
| Home
@endsection


@section('sidebar')
@include('home.sidebar-home')
@endsection


@section('header-content')
@include('home.header')
@endsection


@section('content')
@include('home.content')
@endsection