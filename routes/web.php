<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// LESS Compile
Route::get('{fichero}.less', 'LessController@compile')->where('fichero', '[a-zA-Z0-9\/\-]+');

// Routes Web
Route::get('/', 'HomeController@index');
Route::get('login', 'LoginController@index');
Route::post('logueandose', 'LoginController@login');
Route::get('bienvenido', 'LoginController@welcome');
Route::get('cerrar-sesion', 'LoginController@logout');

// Mails
Route::post('enviandomensaje', 'MailController@sendEmail');

// Search
Route::get('buscador', 'SearchController@getSearchPage');
Route::post('buscador', 'SearchController@search');

// Category
Route::get('{category}', 'CategoryController@getCategory')
	->where('category', 'pruebas|preguntas|documentos|ensenanzas|casos|investigaciones');
Route::get('biblioteca/{category}', 'CategoryController@getCategoryArchive')
	->where('category', 'pruebas|preguntas|documentos|ensenanzas|casos|investigaciones');

// Subcategory
Route::get('{category}/{subcategory}', 'CategoryController@getSubcategory')
	->where('category', 'pruebas|preguntas|documentos|ensenanzas|casos|investigaciones');
Route::get('biblioteca/{category}/{subcategory}', 'CategoryController@getSubcategoryArchive')
	->where('category', 'pruebas|preguntas|documentos|ensenanzas|casos|investigaciones');

// Item
Route::get('{category}/{subcategory}/{path}', 'ItemController@getItem')
	->where('category', 'pruebas|preguntas|documentos|ensenanzas|casos|investigaciones');

// Teacher
Route::get('miembros', 'PageController@getTeachers');
Route::get('profesores', 'PageController@getTeachers');
Route::get('profesores/{path}', 'PageController@getTeacher');

// Static Pages
Route::get('quienes-somos', 'PageController@getAboutUs');
Route::get('cuota-socios', 'PageController@getMembershipPage');
Route::get('admision-socios', 'PageController@getAdmissionMembershipPage');
Route::get('objetivos', 'PageController@getTargetPage');
Route::get('evaluacion-psicologica', 'PageController@getEpPage');
Route::get('diagnostico-psicologico', 'PageController@getDpPage');
Route::get('tipo-evaluacion', 'PageController@getTePage');
Route::get('utilidad', 'PageController@getUtilityPage');
Route::get('proceso', 'PageController@getProcessPage');
Route::get('componentes', 'PageController@getComponentsPage');

// Legal Pages
Route::get('contacto', 'PageController@getContactPage');
Route::get('politica-cookies', 'PageController@getCookiesPage');
Route::get('politica-privacidad', 'PageController@getPrivacyPage');
Route::get('aviso-legal', 'PageController@getLegalPage');
Route::get('creative-commons', 'PageController@getCreativePage');

// 404
Route::any('{catchall}', 'Controller@show404')->where('catchall', '(.*)');
