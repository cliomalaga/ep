<?php

return array(
	'public_path'		=> public_path(env('LESS_PUBLIC_PATH')),
	'link_path'			=> env('LESS_LINK_PATH'),
	'less_path'			=> base_path(env('LESS_FILES_PATH')),
	'cache_dir'			=> storage_path(env('LESS_CACHE_DIR')),
    'compile_frequency'	=> env('LESS_COMPILE_FREQUENCY'),
);